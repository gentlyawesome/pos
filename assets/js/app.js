var app = angular.module("sampleApp", ["firebase"]);
app.controller("SampleCtrl", function($scope, $firebaseObject, $firebaseArray, $http) {
  var ref = new Firebase("https://blinding-inferno-8178.firebaseio.com/pos_tracking");

  // download the data into a local object
  $scope.summaries = $firebaseArray(ref);

  $scope.sendReport = function(sendReportClick){
    $http({
      url: sendReportClick, 
      method: "POST"
    }).success(function(summary){
      $scope.summaries.$add({
        summary
      }); 
      alert("Successfully Sent Report");
    });  
  };

});
