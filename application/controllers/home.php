<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model(array('M_pos_table_categories','M_receipts','M_pos_tables','M_users'));
	}
	
	public function index(){
	  //echo "<pre>";
	  //var_dump($this->session->userdata['userInfo']['username']);
	  //die();

	  $current_user['where']['username'] = $this->session->userdata['userInfo']['username'];
	  $current_user['single'] = TRUE;

	  $current = $this->M_users->get_record('users', $current_user);
	  if(empty($current)){
	    redirect('auth/logout');
	  }

	  $config_user['where']['remove'] = 0;
	  $config_user['single'] = FALSE;

	  $users = $this->M_users->get_record('users', $config_user);

  	  $count = 0; 
	  $user_list = array();
          foreach($users as $u){
	   if(preg_match("/Server/",$u->usertype)){
		$user_list[] = $u;	
	   }
          }
          $this->view_data['users'] = $user_list;

         $config_data['where']['remove'] = 0;
	 $this->view_data['area'] =$area  = $this->M_pos_table_categories->get_record(false, $config_data); 

         if($area){
           foreach($area as $a){
             $config_data2['where']['pos_table_category_id'] = $a->id;
             $config_data2['where']['remove'] = 0;
	     $this->view_data['table'][$a->id]  = $tables = $this->M_pos_tables->get_record(false, $config_data2); 
		  if($tables){
		  foreach($tables as $key => $value){
		   $this->view_data['table_receipt'][$value->id] =  $receipt = $this->M_pos_tables->check_receipt_order($value->id);
		   $this->view_data['table_receipt_group'][$value->id] =  $receipt = $this->M_pos_tables->check_receipt_order($value->id);
		  }
		  } }
	  }
	
	  $config2['join'][] = array('table'=>'pos_table', 'on'=>'pos_table.id = receipts.pos_table_id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'pos_table_categories', 'on'=>'pos_table_categories.id = receipts.pos_table_category_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >'] = date('Y-m-d 00:00:00');
	  $config2['where']['receipts.created_at <'] = date('Y-m-d 24:00:00');
	  $config2['fields'] = array('receipts.*', 'pos_table.name as pos_table_name', 'pos_table_categories.name as pos_table_category_name');
	  $config2['where']['receipts.paid'] = 0;

	  $this->view_data['unpaid_receipts'] = $receipts = $this->M_receipts->get_record(false, $config2);
	}

	public function unpaid_receipt(){
         $config_data['where']['remove'] = 0;
	 $this->view_data['area'] =$area  = $this->M_pos_table_categories->get_record(false, $config_data); 

         if($area){
           foreach($area as $a){
             $config_data2['where']['pos_table_category_id'] = $a->id;
             $config_data2['where']['remove'] = 0;
	     $this->view_data['table'][$a->id]  = $tables = $this->M_pos_tables->get_record(false, $config_data2); 
		  if($tables){
		  foreach($tables as $key => $value){
		   $this->view_data['table_receipt'][$value->id] =  $receipt = $this->M_pos_tables->check_receipt_order($value->id);
		   $this->view_data['table_receipt_group'][$value->id] =  $receipt = $this->M_pos_tables->check_receipt_order_unpaid($value->id);
		  }
		  } }
	  }
	
	  $config2['join'][] = array('table'=>'pos_table', 'on'=>'pos_table.id = receipts.pos_table_id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'pos_table_categories', 'on'=>'pos_table_categories.id = receipts.pos_table_category_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >'] = date('Y-m-d 00:00:00');
	  $config2['where']['receipts.created_at <'] = date('Y-m-d 24:00:00');
	  $config2['fields'] = array('receipts.*', 'pos_table.name as pos_table_name', 'pos_table_categories.name as pos_table_category_name');
	  $config2['where']['receipts.paid'] = 0;

	  $this->view_data['unpaid_receipts'] = $receipts = $this->M_receipts->get_record(false, $config2);
	}

}

