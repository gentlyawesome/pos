<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory extends MY_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model('M_inventory_items');
	  $this->load->helper('url');
	  $this->view_data["userInfo"] = $userInfo =  $this->userInfo;
	}

	public function index()
	{
	  $inventory_items = $this->db->get('inventory_items')->result();

	  $this->view_data["inventory_items"] = $inventory_items; 
	}
}
