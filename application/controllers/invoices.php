<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoices extends MY_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model('M_invoices');
	  $this->load->helper('url');
	  $this->view_data["userInfo"] = $userInfo =  $this->userInfo;
	}

	public function index($from = NULL, $to = NULL)
        {
	  if(!empty($from) && !empty($to)){
            $from = date('Y-m-d',strtotime(str_replace('-', '/', $from)));
	    $to = date('Y-m-d',strtotime(str_replace('-', '/', $to)));

	    $this->db->from('invoices');
	    $this->db->where('invoice_date >= ', $from);
            $this->db->where('invoice_date <= ', $to);
	    $result = $this->db->get();

	    $this->view_data['invoices'] = $result;
          }else{
	    $this->db->from('invoices');
	    $result = $this->db->get();

	    $this->view_data['invoices'] = $result;
	  }

	  if($_POST){
	    $from = $this->input->post('from');
	    $to = $this->input->post('to');
            $from = date('Y-m-d',strtotime(str_replace('-', '/', $from)));
	    $to = date('Y-m-d',strtotime(str_replace('-', '/', $to)));

	   redirect('invoices/index/'.$from.'/'.$to);
            
	  }
	  $this->view_data["from"] = $from;
	  $this->view_data["to"] = $to;

	}

	public function show($id)
	{
	   $this->db->from('invoices');
	   $this->db->where('id', $id);
	   $invoice_result = $this->db->get();
	   $invoice = $invoice_result->row();

	   $this->view_data['invoice'] = $invoice;
	   $this->view_data['check_if_all_items_are_added'] = $this->M_invoices->check_if_all_items_are_added($invoice->id);
	

	   $this->db->from('invoice_items');
	   $this->db->where('invoice_id', $id);
	   $invoice_items_result = $this->db->get();
	   $invoice_items = $invoice_items_result->result();
	  
           $this->view_data['invoice_items'] = $invoice_items;
	}

	public function delete($id, $from = NULL, $to = NULL)
	{
	   $this->db->where('invoice_id', $id)->delete('invoice_items');
	   $this->db->where('id', $id)->delete('invoices');
	   $this->session->set_flashdata('message', 'Successfully Deleted');

	   if(!empty($from) && !empty($to)){
	   redirect('/invoices/index/'.$from.'/'.$to);
	   }else{
	   redirect('/invoices');
	   }

	}

	public function add()
	{
	  $this->load->helper('form');

	  if($this->input->post()){
	  $invoice = array();
	  $invoice['vendor_id'] = $this->input->post('vendor_id'); 
	  $invoice['invoice_number'] = $this->input->post('invoice_number'); 
	  $invoice['invoice_date'] = $this->input->post('invoice_date'); 

	  if(!empty($invoice['vendor_id']))
          {
	    $this->db->insert('invoices', $invoice);
	    $id = $this->db->insert_id();
	  }

	  $invoice_items = array();
	  for($i=1; $i < 10; $i++) {
		  if(!empty($this->input->post('item_number_'.$i))){ 
		  $invoice_items["invoice_id".$i] = $id;
		  $invoice_items["item_number".$i] = $this->input->post('item_number_'.$i); 
		  $invoice_items["item_description".$i] = $this->input->post('item_description_'.$i); 
		  $invoice_items["quantity_in_stock".$i] = $this->input->post('quantity_in_stock_'.$i); 
		  $invoice_items["type_of_item".$i] = $this->input->post('type_of_item_'.$i); 
		  $invoice_items["minimum_order_quantity".$i] = $this->input->post('minimum_order_quantity_'.$i); 
		  }
          }
	  $invoice_item_groups = array_chunk($invoice_items, 6);
	  if(!empty($invoice_item_groups)){
	  foreach($invoice_item_groups as $data){
	  $invoice_item = array();
		  $invoice_items_group["invoice_id"] = $id;
		  $invoice_items_group["item_number"] = $data[1]; 
		  $invoice_items_group["item_description"] = $data[2]; 
		  $invoice_items_group["quantity_in_stock"] = $data[3];
		  $invoice_items_group["type_of_item"] = $data[4];
		  $invoice_items_group["minimum_order_quantity"] = $data[5];
	  $this->db->insert('invoice_items', $invoice_items_group);
	  }
	  }

	   $this->session->set_flashdata('message', 'Successfully Added!');
	   redirect('/invoices/show/'.$id);


	  }
	}

	public function revert_items($id)
	{
	   $this->db->from('inventory_items');
	   $this->db->where('invoice_id', $id);
	   $inventory_items_result = $this->db->get();
	   $inventory_items = $inventory_items_result->result();
	
	   foreach($inventory_items as $ii)
	   {
		$this->db->delete('inventory_items', array('id'=> $ii->id)) ;
	        $this->db->where('id', $ii->invoice_item_id)->update('invoice_items', array('quantity_added' => 0));	   
	   }

	   $this->db->where('id', $id)->update('invoices', array('quantity_added' => 0));	   

	   $this->session->set_flashdata('message', 'Successfully Reverted');
	   redirect('invoices/show/'.$id);
	}

	public function revert_item($id)
	{
	   $this->db->delete('inventory_items', array('invoice_item_id'=> $id)) ;

	   $this->db->from('invoice_items');
	   $this->db->where('id', $id);
	   $invoice_item_result = $this->db->get();
	   $invoice_item = $invoice_item_result->row();

	   $this->db->where('id', $id)->update('invoice_items', array('quantity_added' => 0));	   

	   $this->session->set_flashdata('message', 'Successfully Reverted');
	   redirect('invoices/show/'.$invoice_item->invoice_id);
	}



}
