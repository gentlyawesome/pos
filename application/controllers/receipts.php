<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receipts extends MY_Controller {
	protected $userInfo;
	public function __construct(){
	  parent::__construct();
	  $this->load->model(array('M_pos_tables','M_pos_table_categories','M_receipts', 'M_items', 'M_orders', 'M_users'));
	  $this->view_data["userInfo"] = $userInfo =  $this->userInfo;
	}

	public function create($pos_table_category_id, $pos_table_id, $server){
	   $data["user_id"] = $this->userInfo['user_id'];
	   $data["pos_table_id"] = $pos_table_id;
	   $data["pos_table_category_id"] = $pos_table_category_id;
	   $data["server_open"] = $server;
	
	   $process = $this->M_receipts->create($data);
	   if($process['status']){
	     redirect('receipts/view/'.$process['id']);
	   }

	}

	public function view($id){
	  $config_user['where']['remove'] = 0;
	  $config_user['single'] = FALSE;

	  $users = $this->M_users->get_record('users', $config_user);

  	  $count = 0; 
	  $user_list = array();
          foreach($users as $u){
	   if(preg_match("/Server/",$u->usertype)){
		$user_list[] = $u;	
	   }
          }
          $this->view_data['users'] = $user_list;

	  $config['where']['receipts.id'] = $id;
	  $config['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config['fields'] = array('receipts.*', 'pos_table.name');
	  $config['single'] = TRUE;

	  $this->view_data['receipt'] = $receipt  = $this->M_receipts->get_record(false, $config);

	  $config4['where']['receipts.id'] = $id;
	  $config4['where']['receipts.paid'] = 1;
	  $config4['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config4['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config4['fields'] = array('receipts.*', 'orders.*', 'orders.id as orderid','pos_table.name');
	  $config4['single'] = FALSE;

	  $this->view_data['receipt_paid'] = $receipt_paid  = $this->M_receipts->get_record(false, $config4);

	  if($receipt_paid){
	  foreach($receipt_paid as $rp){
	  $this->view_data['order_paid'][$rp->orderid] = $rp;
	  }
	  }

	    $item_config['single'] = FALSE;
	    $item_config['group'] = 'category';

	    $item_config2['single'] = FALSE;
	    $this->view_data['items'] = $items =  $this->M_items->get_record(false, $item_config);

	    $this->view_data['items2'] = $items2 =  $this->M_items->get_record(false, $item_config2);
            foreach($items2 as $i):  
             $this->view_data['item_category'][$i->id] = $i->category;
             $this->view_data['item'][$i->category][] = $i;
            endforeach;
	    $order_config['where']['receipt_id'] = $id;
	    $order_config['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    $order_config['fields'] = array('items.*','orders.discount_value', 'orders.count','orders.id as orderid');

	    $this->view_data['orders'] = $this->M_orders->get_record(false, $order_config);

	  $user_auth_status = FALSE;
	  if(!$receipt->paid){
	    if($_POST){
	    //if($this->input->post('post_name') == 'discount'){
	    //var_dump($_POST); die();
	    //}else{
	  $config_auth['where']['id'] = $this->input->post('server'); 
	  $config_auth['where']['password'] = $this->input->post('serverpass'); 
	  $config_auth['single'] = TRUE;

	  $user_auth = $this->M_users->get_record('users', $config_auth);
          if($user_auth){
	       $user_auth_status = TRUE;
	  }

	       $data['or_no'] = $this->input->post('or_no');  
	       $data['created_at'] = $this->input->post('created_at');  
	       $data['payment'] = floatval($this->input->post('cash_handed_value')); 
	       $data['change'] = floatval($this->input->post('change_value')); 
	       $data['discount'] = $this->input->post('discount'); 
	       $data['discount_remarks'] = $this->input->post('discount_remarks'); 
	       $data['number_of_guest'] = $this->input->post('number_of_guest'); 
	       $data['open_price'] = $this->input->post('open_price'); 
	       $data['open_discount'] = $this->input->post('open_discount'); 
	       $data['open_price_remarks'] = $this->input->post('open_price_remarks'); 
	       $data['server'] = $user_auth->name;  
	       $data['paid'] = 1; 

	      $discount = 0;
	      $total_items = 0;
	      $order_ids = $this->input->post('order_ids');
	      foreach($order_ids as $key=>$value){
	      $exist['where']["id"] = $key;
	      $exist['single'] = TRUE;

              $existorder = $this->M_orders->get_record(false, $exist);

	      $item['where']['id'] = $existorder->item_id;
	      $item['single'] = TRUE;

	      $existitem = $this->M_items->get_record(false, $item);

	       $order['discount_percent'] = $value; 
	       $order['discount_value'] = floatval((floatval($value)/ 100.0) * (floatval($existitem->value) * floatval($existorder->count))); 
	       $discount += $order['discount_value'];

	       $this->M_orders->update($key, $order);
	      }

	      $order_discount_ids = $this->input->post('order_discount_ids');
	      foreach($order_discount_ids as $key=>$value){
	      $exist1['where']["id"] = $key;
	      $exist1['single'] = TRUE;

              $existorder1 = $this->M_orders->get_record(false, $exist1);

	      $item1['where']['id'] = $existorder1->item_id;
	      $item1['single'] = TRUE;

	      $existitem1 = $this->M_items->get_record(false, $item1);

	       $order1['discount_count'] = $value; 

	       $this->M_orders->update($key, $order1);
	      }


	      if($user_auth_status){
	      $data['discount'] = $discount;

	      $this->M_receipts->update($receipt->id, $data);


	      redirect('receipts/receipt_print/'.$receipt->id);
	     }else{
	      $this->session->set_flashdata('msg', '<div class="alert alert-danger">Wrong Password, Please Try again.</div>');
	      redirect('receipts/view/'.$receipt->id);
	     }
	   // }
	  }
	}

	}

	public function view_paid($id){
	  $config['where']['receipts.id'] = $id;
	  $config['where']['receipts.paid'] = 1 ;
	  $config['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config['fields'] = array('receipts.*', 'pos_table.name');
	  $config['single'] = TRUE;

	  $this->view_data['receipt']  = $this->M_receipts->get_record(false, $config);
	      
	    $order_config['where']['receipt_id'] = $id;
	    $order_config['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    $order_config['fields'] = array('items.*');

	    $this->view_data['orders'] = $this->M_orders->get_record(false, $order_config);

	}


	public function pay($id){
	  $config['where']['receipts.id'] = $id;
	  $config['single'] = TRUE;

	  $this->view_data['receipt'] = $receipt = $this->M_receipts->get_record(false, $config);
	  
	  $data['paid'] = 1;
	  if($this->M_receipts->update($id, $data)){
	    redirect('receipts/receipt_print/'.$id);
          }

	}

	public function today(){
	  if($_POST){
	  $today = $this->input->post('today');
	  $config2['join'][] = array('table'=>'pos_table', 'on'=>'pos_table.id = receipts.pos_table_id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'pos_table_categories', 'on'=>'pos_table_categories.id = receipts.pos_table_category_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >='] = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $today)));
	  $config2['where']['receipts.created_at <='] = date('Y-m-d 23:59:00',strtotime(str_replace('-', '/', $today)));
	  $config2['fields'] = array('receipts.*','receipts.id as receiptid', 'pos_table.name as pos_table_name', 'pos_table_categories.name as pos_table_category_name');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_receipts'] = $receipts = $this->M_receipts->get_record(false, $config2);

          //echo "<pre>";
          //var_dump($receipts); die();

	  if($receipts){
	  $num = 0;
	  foreach($receipts as $r){
	  $this->view_data['receipt_value'][$r->id] = 0;
	    ${'order_config'.$num}['where']['orders.receipt_id'] = $r->receiptid;
	    ${'order_config'.$num}['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    ${'order_config'.$num}['fields'] = array('items.*','items.value as itemvalue','orders.*');

	    $orders = $this->M_orders->get_record(false, ${'order_config'.$num});
	    $num ++;
	    
	    
	    if($orders){
	    $orders_discount = 0;
	    $orders_total = 0;
	    $orders_final = 0;
	    foreach($orders as $o){
               $orders_discount += $o->discount_value;               
               $orders_total += ($o->itemvalue * $o->count) ;               
	    }
	       $orders_final += $orders_total - $orders_discount;
	    }
	  $this->view_data['receipt_value'][$r->id] += ($orders_final + $r->open_price) - $r->open_discount;
	  }

	  }
	  }else{
	  $today = date('Y-m-d');
	  $config2['join'][] = array('table'=>'pos_table', 'on'=>'pos_table.id = receipts.pos_table_id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'pos_table_categories', 'on'=>'pos_table_categories.id = receipts.pos_table_category_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >='] = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $today)));
	  $config2['where']['receipts.created_at <='] = date('Y-m-d 23:59:00',strtotime(str_replace('-', '/', $today)));
	  $config2['fields'] = array('receipts.*','receipts.id as receiptid', 'pos_table.name as pos_table_name', 'pos_table_categories.name as pos_table_category_name');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_receipts'] = $receipts = $this->M_receipts->get_record(false, $config2);


	  if($receipts){
	  $num = 0;
	  foreach($receipts as $r){
	  $this->view_data['receipt_value'][$r->id] = 0;
	    ${'order_config'.$num}['where']['orders.receipt_id'] = $r->receiptid;
	    ${'order_config'.$num}['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    ${'order_config'.$num}['fields'] = array('items.*','items.value as itemvalue','orders.*');

	    $orders = $this->M_orders->get_record(false, ${'order_config'.$num});
	    $num ++;
	    

	    if($orders){
	    $orders_discount = 0;
	    $orders_total = 0;
	    $orders_final = 0;
	    foreach($orders as $o){
               $orders_discount += $o->discount_value;               
               $orders_total += ($o->itemvalue * $o->count) ;               
	    }
	       $orders_final += (($orders_total - $orders_discount) - $r->open_discount) + $r->open_price;
	    }
	    
	  $this->view_data['receipt_value'][$r->id] = $orders_final;

	  }

	  }
	  }
	
	$this->view_data['today'] = $today;



	}

	public function today_sales(){
	  $config2['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >='] = date('Y-m-d 00:00:00');
	  $config2['where']['receipts.created_at <='] = date('Y-m-d 24:00:00');
	  $config2['fields'] = array('receipts.*', 'orders.*', 'items.*');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_items'] = $receipts = $this->M_receipts->get_record(false, $config2);

	}

	public function monthly_sales(){

   	  if($_POST){
	  $this->view_data['date1'] = $date1 = $this->input->post('date1'); 
	  $this->view_data['date2'] = $date2 = $this->input->post('date2'); 

	  $config2['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >='] = date($date1 . ' 00:00:00');
	  $config2['where']['receipts.created_at <='] = date($date2 . ' 24:00:00');
	  $config2['fields'] = array('receipts.*', 'orders.*', 'items.*');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_items'] = $receipts = $this->M_receipts->get_record(false, $config2);
	  }

	}

	public function monthly_sales_print($date1, $date2){

	  $config2['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config2['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config2['where']['receipts.created_at >='] = date($date1 . ' 00:00:00');
	  $config2['where']['receipts.created_at <='] = date($date2 . ' 24:00:00');
	  $config2['fields'] = array('receipts.*', 'orders.*', 'items.*');
	  $config2['where']['receipts.paid'] = 1;

	$data['paid_items'] = $receipts = $this->M_receipts->get_record(false, $config2);
	$data['date'] = date('m/d/Y h:i:s');
	$data['date1'] = $date1; 
	$data['date2'] = $date2; 

	$this->load->library('mpdf');
	$html = $this->load->view('printables/monthly_sales_print', $data, true);	
	//echo $html;
	//$this->layout->view("empty");
        ////$mpdf=new mPDF('','FOLIO','','',1,1,1,1,0,0); 
	////$mpdf->WriteHTML($html);
	////$mpdf->Output();

	////exit;

	}


	public function today_top_sales(){
	  $sql = "Select distinct category
		  from items";
	  $query = $this->db->query($sql);
	  $this->view_data['item_categories'] = $item_categories = $query->num_rows() > 0 ? $query->result() : NULL; 

	  if(!empty($item_categories)){
	  $counter = 0;
          foreach($item_categories as $ic){
	  $counter ++;
	  ${'config'.$counter}['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  ${'config'.$counter}['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  ${'config'.$counter}['where']['receipts.created_at >='] = date('Y-m-d 00:00:00');
	  ${'config'.$counter}['where']['receipts.created_at <='] = date('Y-m-d 24:00:00');
	  ${'config'.$counter}['where']['items.category '] = $ic->category;
	  ${'config'.$counter}['fields'] = array('receipts.*', 'orders.*', 'items.*','SUM(orders.count) as amount_sold');
	  ${'config'.$counter}['where']['receipts.paid'] = 1;

	  $this->view_data['item'][$ic->category]  = $this->M_receipts->get_record(false, ${'config'.$counter});
	  }
	  }

	}

	public function transfer_select_table_category($id){
	 $this->view_data['id'] = $id;
	 $tables  = $this->M_pos_table_categories->get_record(false); 
	
	 if($tables){
         $division = 3;
	 $this->view_data['table_division'] = 12/$division;
	 $data = array_chunk($tables, $division);

	 $this->view_data['tables'] = $data; 	

	 }
	}

	public function transfer_table_category($id, $pos_table_category_id){

	  $data['pos_table_category_id'] = $pos_table_category_id;

	   $process = $this->M_receipts->update($id, $data);
	   if($process){
	     redirect('receipts/transfer_select_table/'.$id.'/'.$pos_table_category_id);
	   }

	}

	public function transfer_select_table($id, $pos_table_category_id){
	  $config['where']['id'] = $pos_table_category_id;
	  $config['single'] = TRUE;
   
	  $this->view_data['pos_table_category'] = $table = $this->M_pos_table_categories->get_record(false, $config);

	  $table_config['where']['pos_table_category_id'] = $pos_table_category_id;

	  $tables  = $this->M_pos_tables->get_record(false, $table_config); 

          if($tables){
          $division = 4;
	  $this->view_data['table_division'] = 12/$division;
	  $data = array_chunk($tables, $division);

	  $this->view_data['tables'] = $data; 
	  }


	 $this->view_data['id'] = $id;
	}

	public function transfer_table($id, $pos_table_id){
	  $data['pos_table_id'] = $pos_table_id;
	  $config['single'] = TRUE;

	   $process = $this->M_receipts->update($id, $data);
	   if($process){
	     redirect('receipts/view/'.$id);
	   }

	}

	public function receipt_print($id){
	  $config['where']['receipts.id'] = $id;
	  $config['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config['fields'] = array('receipts.*', 'pos_table.name');
	  $config['single'] = TRUE;

	  $data['receipt'] = $receipt  = $this->M_receipts->get_record(false, $config);

	  $config4['where']['receipts.id'] = $id;
	  $config4['where']['receipts.paid'] = 1;
	  $config4['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config4['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config4['fields'] = array('receipts.*', 'orders.*', 'orders.id as orderid','pos_table.name');
	  $config4['single'] = FALSE;

	  $data['receipt_paid'] = $receipt_paid  = $this->M_receipts->get_record(false, $config4);

	  if($receipt_paid){
	  foreach($receipt_paid as $rp){
	  $data['order_paid'][$rp->orderid] = $rp;
	  }
	  }

	    $item_config['single'] = FALSE;
	    $item_config['group'] = 'category';

	    $item_config2['single'] = FALSE;
	    $this->view_data['items'] = $items =  $this->M_items->get_record(false, $item_config);

	    $this->view_data['items2'] = $items2 =  $this->M_items->get_record(false, $item_config2);
            foreach($items2 as $i):  
             $this->view_data['item_category'][$i->id] = $i->category;
             $this->view_data['item'][$i->category][] = $i;
            endforeach;
	    $order_config['where']['receipt_id'] = $id;
	    $order_config['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    $order_config['fields'] = array('items.*','orders.discount_value', 'orders.count','orders.id as orderid');

	    $data['orders'] = $this->M_orders->get_record(false, $order_config);

	$data['date'] = date('m/d/Y h:i:s');

	$this->load->library('mpdf');
	$html = $this->load->view('printables/receipt_print', $data, true);	
	echo $html;
	$this->layout->view("empty");
        //$mpdf=new mPDF('','FOLIO','','',1,1,1,1,0,0); 
	//$mpdf->WriteHTML($html);
	//$mpdf->Output();

	//exit;
	}

	public function receipt_order($id, $type){
	  $config['where']['receipts.id'] = $id;
	  $config['join'][] = array('table'=>'pos_table', 'on'=> 'pos_table.id = receipts.pos_table_id', 'type'=>'left');
	  $config['fields'] = array('receipts.*', 'pos_table.name');
	  $config['single'] = TRUE;

	  $data['receipt'] = $receipt  = $this->M_receipts->get_record(false, $config);

	    $item_config['single'] = FALSE;
	    $this->view_data['items'] = $this->M_items->get_record(false, $item_config);
	    $order_config['where']['receipt_id'] = $id;
	    $order_config['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id AND items.area_category = "'.$type.'"', 'type'=>'left');
	    $order_config['fields'] = array('items.*', 'orders.count','orders.id as orderid');

	    $data['orders'] = $this->M_orders->get_record(false, $order_config);

	$data['date'] = date('m/d/Y h:i:s');
	$data['area_category'] = $type;

	$this->load->library('mpdf');
	$html = $this->load->view('printables/receipt_order', $data, true);	
	echo $html;
	$this->layout->view("empty");
        //$mpdf=new mPDF('','FOLIO','','',1,1,1,1,0,0); 
	//$mpdf->WriteHTML($html);
	//$mpdf->Output();

	//exit;
	}

	public function print_report(){
	  $sql = "Select distinct name
		  from items";
	  $query = $this->db->query($sql);
	  $data['item_categories'] = $item_categories = $query->num_rows() > 0 ? $query->result() : NULL; 

	  if(!empty($item_categories)){
	  $counter = 0;
          foreach($item_categories as $ic){
	  $counter ++;
	  ${'config'.$counter}['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  ${'config'.$counter}['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  ${'config'.$counter}['where']['receipts.created_at >='] = date('Y-m-d 00:00:00');
	  ${'config'.$counter}['where']['receipts.created_at <='] = date('Y-m-d 24:00:00');
	  ${'config'.$counter}['where']['items.name '] = $ic->name;
	  ${'config'.$counter}['fields'] = array('receipts.*', 'orders.*', 'items.*','SUM(orders.count) as amount_sold');
	  ${'config'.$counter}['where']['receipts.paid'] = 1;

	  $data['item'][$ic->name]  = $this->M_receipts->get_record(false, ${'config'.$counter});
	  }
	  }

	$data['date'] = date('m/d/Y h:i:s');

	$this->load->library('mpdf');
	$html = $this->load->view('printables/print_report', $data, true);	
	echo $html;
	$this->layout->view("empty");
        //$mpdf=new mPDF('','FOLIO','','',1,1,1,1,0,0); 
	//$mpdf->WriteHTML($html);
	//$mpdf->Output();

	//exit;
	}


	public function print_report2(){
	  $sql = "Select distinct category
		  from items";
	  $query = $this->db->query($sql);
	  $data['item_categories'] = $item_categories = $query->num_rows() > 0 ? $query->result() : NULL; 
	    foreach($item_categories as $ic){
	      $data["category"][$ic->category] = 0.0; 
	    }

	  $receipt['where']['created_at >='] = date('Y-m-d 00:00:00');
	  $receipt['where']['created_at <='] = date('Y-m-d 24:00:00');
	  $receipt['where']['paid'] = 1;

	  $receipts = $this->M_receipts->get_record(false, $receipt);
          
	  $counter = 0;
	  $orders_discount =$orders_sum= 0;
	  foreach($receipts as $r){
	    ${'order'.$counter}['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	    ${'order'.$counter}['fields'] = array('orders.*', 'items.*', 'items.value as itemvalue');
	    ${'order'.$counter}['where']['orders.receipt_id'] = $r->id;  

	    ${'order_result'.$counter} = $this->M_orders->get_record(false, ${'order'.$counter});


	    foreach(${'order_result'.$counter} as $order){
	      $orders_discount += $order->discount_value ; 
	      $orders_sum += $order->count * $order->itemvalue;
	      $data["category"][$order->category] += $order->count * $order->itemvalue; 
	    }
	    $counter ++;
	  }

	  $data['category_discount'] = $orders_discount;
	  $data['sum'] = $orders_sum - $orders_discount;



	$data['date'] = date('m/d/Y h:i:s');

	$this->load->library('mpdf');
	$html = $this->load->view('printables/print_report2', $data, true);	
	echo $html;
	$this->layout->view("empty");
        //$mpdf=new mPDF('','FOLIO','','',1,1,1,1,0,0); 
	//$mpdf->WriteHTML($html);
	//$mpdf->Output();

	//exit;
	}

	public function cancel($id){
	  $data["remove"] = 1;
	  $this->M_receipts->update($id, $data);

	  redirect('home');

	}

	private function getTop( $from, $to, $type = "bar" )
	{
	  $counter = array();
	  $receipts = $this->db->get_where('receipts', array('created_at >= ' => $from, 'created_at <= ' => $to, 'paid' => 1));
	  
	  foreach( $receipts->result() as $receipt ){
	     $orders = $this->db->get_where('orders', array('receipt_id' => $receipt->id));
	     foreach( $orders->result() as $order ){
	      $item = $this->db->get_where('items', array('id' => $order->item_id));
	      if( $item->row()->area_category == $type ){
	        if( !isset($counter[$item->row()->name]) ){
	           $counter[$item->row()->name] = 0;
		}

	        if( isset($counter[$item->row()->name]) ){
	           $counter[$item->row()->name] += $order->count;
		}
		
	      }
	     }
	  } 

	 arsort($counter);
	 $counter = array_slice($counter, 0, 10);
	 return $counter;
	}

	public function summary(){
	  if($_POST){
	  $from = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $this->input->post('from'))));
	  $to = date('Y-m-d 23:59:59',strtotime(str_replace('-', '/', $this->input->post('to'))));
	  $config['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config['where']['receipts.created_at >='] = $from; 
	  $config['where']['receipts.created_at <='] = $to; 
	  $config['fields'] = array('receipts.*','orders.count', 'items.area_category', 'items.name', 'items.value');
	  $config['where']['receipts.paid'] = 1;

	  $this->view_data['receipts'] = $receipts = $this->M_receipts->get_record(false, $config);

	  $config2['where']['receipts.created_at >='] = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $from)));
	  $config2['where']['receipts.created_at <='] = date('Y-m-d 23:59:00',strtotime(str_replace('-', '/', $to)));
	  $config2['fields'] = array('receipts.*');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_receipts'] = $paid_receipts = $this->M_receipts->get_record(false, $config2);
	  $total_sale = 0;
	  $total_disc = 0;


	  if($paid_receipts){
	  $num = 0;
	  foreach($paid_receipts as $r){
	  $this->view_data['receipt_value'][$r->id] = 0;
	    ${'order_config'.$num}['where']['orders.receipt_id'] = $r->id;
	    ${'order_config'.$num}['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    ${'order_config'.$num}['fields'] = array('items.*','items.value as itemvalue','orders.*');

	    $orders = $this->M_orders->get_record(false, ${'order_config'.$num});
	    $num ++;
	    

	    if($orders){
	    $orders_discount = 0;
	    $orders_total = 0;
	    $orders_final = 0;
	    foreach($orders as $o){
               $orders_discount += $o->discount_value;               
               $orders_total += ($o->itemvalue * $o->count) ;               
	    }
	       $orders_final += $orders_total - $orders_discount;
	    }
	  $total_sale += ($orders_final + $r->open_price) - $r->open_discount;
	  $total_disc += $orders_discount;
	    

	  }

	  }

	
	  


	  $highest_server = array();
	  $highest_selling_beverage = array();
	  $highest_selling_food = array();
	  $num_guest = 0;
	  $receipt_order = array();
	  $highest_food = array();
	  $highest_beverage = array();
	  $highest_other = array();
	  $number_of_guest = 0;
	  $list_of_others = array();
	  $vip = 0;

	  $total_food_sale = 0;
	  $total_beverage_sale = 0;
	  $total_discount = 0;
	  $total_sales = 0;

	  foreach($receipts as $receipt){
	     $list_of_close_servers[] = $receipt->server; 
	     $list_of_open_servers[] = $receipt->server_open; 
	     $total_discount += $receipt->discount;

	     if ($receipt->area_category != "bar") {
	       if(preg_match('/vip/', strtolower($receipt->name))){
	         $list_of_others[] = $receipt->name;
	       }else{
	         $list_of_foods[] = $receipt->name;
	       }
	         $total_food_sale += $receipt->count * $receipt->value;
	     }else{
	       $list_of_beverages[] = $receipt->name;
               $total_beverage_sale += $receipt->count * $receipt->value;
	     }
	     $total_sales += $receipt->count * $receipt->value;

	     if(!in_array($receipt->or_no, $receipt_order)){
	      $receipt_order[] = $receipt->or_no;
	      $num_guest += $receipt->number_of_guest;
	     }

	     $number_of_guest = $num_guest;
	  }

	  

	  $highest_close_server_count = array_count_values(array_filter($list_of_close_servers));
	  if(!empty(array_filter($list_of_close_servers))){
	    $highest_close_server = array_search(max($highest_close_server_count), $highest_close_server_count);
	  }else{
	    $highest_close_server = "None";
	  }

	  $highest_open_server_count = array_count_values(array_filter($list_of_open_servers));
	  if(!empty(array_filter($list_of_open_servers))){
	    $highest_open_server = array_search(max($highest_open_server_count), $highest_open_server_count);
	  }else{
	    $highest_open_server = "None";
	  }

	  $this->view_data['from'] = $from;
	  $this->view_data['to'] = $to;
	  $this->view_data['raw_from'] = $this->input->post('from');
	  $this->view_data['raw_to'] = $this->input->post('to');

	  $this->view_data['highest_close_server'] = $highest_close_server;
	  $this->view_data['highest_open_server'] = $highest_open_server;
	  $this->view_data['total_number_of_guest'] = $number_of_guest;
	  $this->view_data['total_beverage_sale'] = $total_beverage_sale;
	  $this->view_data['total_discount'] = $total_discount;
	  $this->view_data['total_food_sale'] = $total_food_sale;
	  $this->view_data['total_sales'] = $total_sales - $total_discount;
	  $this->view_data['total_sale'] = $total_sale;
	  $this->view_data['total_disc'] = $total_disc;
	  $this->view_data['top_foods'] = $this->getTop( $from, $to, "kitchen" );
	  $this->view_data['top_beverages'] = $this->getTop( $from, $to, "bar" );
	  }

	
	}


	public function summary_print($from, $to){
	  $config['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config['where']['receipts.created_at >='] = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $from)));
	  $config['where']['receipts.created_at <='] = date('Y-m-d 23:59:00',strtotime(str_replace('-', '/', $to)));
	  $config['fields'] = array('receipts.*','orders.count', 'items.area_category', 'items.name', 'items.value');
	  $config['where']['receipts.paid'] = 1;

	  $this->view_data['receipts'] = $receipts = $this->M_receipts->get_record(false, $config);

	  $config2['where']['receipts.created_at >='] = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $from)));
	  $config2['where']['receipts.created_at <='] = date('Y-m-d 23:59:00',strtotime(str_replace('-', '/', $to)));
	  $config2['fields'] = array('receipts.*');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_receipts'] = $paid_receipts = $this->M_receipts->get_record(false, $config2);
	  $total_sale = 0;
	  $total_disc = 0;


	  if($paid_receipts){
	  $num = 0;
	  foreach($paid_receipts as $r){
	  $this->view_data['receipt_value'][$r->id] = 0;
	    ${'order_config'.$num}['where']['orders.receipt_id'] = $r->id;
	    ${'order_config'.$num}['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    ${'order_config'.$num}['fields'] = array('items.*','items.value as itemvalue','orders.*');

	    $orders = $this->M_orders->get_record(false, ${'order_config'.$num});
	    $num ++;
	    

	    if($orders){
	    $orders_discount = 0;
	    $orders_total = 0;
	    $orders_final = 0;
	    foreach($orders as $o){
               $orders_discount += $o->discount_value;               
               $orders_total += ($o->itemvalue * $o->count) ;               
	       $orders_total += $r->open_price;
	    }
	       $orders_final += ($orders_total - $orders_discount) - $r->open_discount;
	    }
	  $total_sale += $orders_total;
	  $total_disc += $orders_discount;
	    

	  }

	  }


	  $highest_server = array();
	  $highest_selling_beverage = array();
	  $highest_selling_food = array();
	  $num_guest = 0;
	  $receipt_order = array();
	  $highest_food = array();
	  $highest_beverage = array();
	  $highest_other = array();
	  $number_of_guest = 0;
	  $list_of_others = array();
	  $vip = 0;

	  $total_food_sale = 0;
	  $total_beverage_sale = 0;
	  $total_discount = 0;
	  $total_sales = 0;

	  foreach($receipts as $receipt){
	     $list_of_servers[] = $receipt->server; 
	     $total_discount += $receipt->discount;

	     if ($receipt->area_category != "bar") {
	       if(preg_match('/vip/', strtolower($receipt->name))){
	         $list_of_others[] = $receipt->name;
	       }else{
	         $list_of_foods[] = $receipt->name;
	       }
	         $total_food_sale += $receipt->count * $receipt->value;
	     }else{
	       $list_of_beverages[] = $receipt->name;
               $total_beverage_sale += $receipt->count * $receipt->value;
	     }
	     $total_sales += $receipt->count * $receipt->value;

	     if(!in_array($receipt->or_no, $receipt_order)){
	      $receipt_order[] = $receipt->or_no;
	      $num_guest += $receipt->number_of_guest;
	     }

	     $number_of_guest = $num_guest;
	  }


	  $highest_food_count = array_count_values(array_filter($list_of_foods));
	  $list_of_foods_filled = array_replace($list_of_foods, array_fill_keys(array_keys($list_of_foods, null), ''));
	  $highest_selling_food = array_count_values($list_of_foods_filled);
	  arsort($highest_selling_food);
	  $highest_selling_food = array_slice($highest_selling_food, 0, 10);


	  if(!empty(array_filter($list_of_foods))){
	    $highest_food = array_search(max($highest_food_count), $highest_food_count);
	  }else{
	    $highest_food = "None";
	  }

	  $highest_other_count = array_count_values(array_filter($list_of_others));
	  if(!empty(array_filter($list_of_others))){
	    $highest_other = array_search(max($highest_other_count), $highest_other_count);
	  }else{
	    $highest_other = "None";
	    }
	  if(!empty($list_of_beverages)){
	  $highest_beverage_count = array_count_values(array_filter($list_of_beverages));

	  $highest_selling_beverage = array_count_values($list_of_beverages);
	  arsort($highest_selling_beverage);
	  $highest_selling_beverage = array_slice($highest_selling_beverage, 0, 10);
	  
	  if(!empty(array_filter($list_of_beverages))){
	    $highest_beverage = array_search(max($highest_beverage_count), $highest_beverage_count);
	  }
	  }else{
	    $highest_beverage = "None";
	    }

	  $highest_server_count = array_count_values(array_filter($list_of_servers));
	  if(!empty(array_filter($list_of_servers))){
	    $highest_server = array_search(max($highest_server_count), $highest_server_count);
	  }else{
	    $highest_server = "None";
	    }

	  $data['from'] = $from;
	  $data['to'] = $to;
	  $data['highest_selling_food'] = $highest_selling_food;
	  $data['highest_selling_beverage'] = $highest_selling_beverage;
	  $data['highest_other'] = $highest_other;
	  $data['highest_food'] = $highest_food;
	  $data['highest_beverage'] = $highest_beverage;
	  $data['highest_server'] = $highest_server;
	  $data['total_number_of_guest'] = $number_of_guest;
	  $data['total_beverage_sale'] = $total_beverage_sale;
	  $data['total_discount'] = $total_discount;
	  $data['total_food_sale'] = $total_food_sale;
	  $data['total_sales'] = $total_sales - $total_discount;
	  $data['total_sale'] = $total_sale - $total_disc;
	  $data['total_disc'] = $total_disc;

	$this->load->library('mpdf');
	$html = $this->load->view('printables/summary_print', $data, true);	
	echo $html;
	$this->layout->view("empty");

	
	}

}

