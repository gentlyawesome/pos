<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_report extends CI_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model(array('M_pos_tables','M_pos_table_categories','M_receipts', 'M_items', 'M_orders', 'M_users'));
	}

	public function report($from, $to){
	  $from = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $from)));
	  $to = date('Y-m-d 23:59:59',strtotime(str_replace('-', '/', $to)));
	  $config['join'][] = array('table'=>'orders', 'on'=>'orders.receipt_id = receipts.id', 'type'=>'left'); 
	  $config['join'][] = array('table'=>'items', 'on'=>'items.id = orders.item_id', 'type'=>'left'); 
	  $config['where']['receipts.created_at >='] = $from; 
	  $config['where']['receipts.created_at <='] = $to; 
	  $config['fields'] = array('receipts.*','orders.count', 'items.area_category', 'items.name', 'items.value');
	  $config['where']['receipts.paid'] = 1;

	  $this->view_data['receipts'] = $receipts = $this->M_receipts->get_record(false, $config);

	  $config2['where']['receipts.created_at >='] = date('y-m-d 00:00:00',strtotime(str_replace('-', '/', $from)));
	  $config2['where']['receipts.created_at <='] = date('y-m-d 23:59:00',strtotime(str_replace('-', '/', $to)));
	  $config2['fields'] = array('receipts.*');
	  $config2['where']['receipts.paid'] = 1;

	  $this->view_data['paid_receipts'] = $paid_receipts = $this->M_receipts->get_record(false, $config2);
	  $total_sale = 0;
	  $total_disc = 0;


	  if($paid_receipts){
	  $num = 0;
	  foreach($paid_receipts as $r){
	  $this->view_data['receipt_value'][$r->id] = 0;
	    ${'order_config'.$num}['where']['orders.receipt_id'] = $r->id;
	    ${'order_config'.$num}['join'][] = array('table'=>'items', 'on'=> 'items.id = orders.item_id', 'type'=>'left');
	    ${'order_config'.$num}['fields'] = array('items.*','items.value as itemvalue','orders.*');

	    $orders = $this->M_orders->get_record(false, ${'order_config'.$num});
	    $num ++;
	    

	    if($orders){
	    $orders_discount = 0;
	    $orders_total = 0;
	    $orders_final = 0;
	    foreach($orders as $o){
               $orders_discount += $o->discount_value;               
               $orders_total += ($o->itemvalue * $o->count) ;               
	    }
	       $orders_final += $orders_total - $orders_discount;
	    }
	  $total_sale += ($orders_final + $r->open_price) - $r->open_discount;
	  $total_disc += $orders_discount;
	    

	  }

	  }

	
	  


	  $highest_server = array();
	  $highest_selling_beverage = array();
	  $highest_selling_food = array();
	  $num_guest = 0;
	  $receipt_order = array();
	  $highest_food = array();
	  $highest_beverage = array();
	  $highest_other = array();
	  $number_of_guest = 0;
	  $list_of_others = array();
	  $vip = 0;

	  $total_food_sale = 0;
	  $total_beverage_sale = 0;
	  $total_discount = 0;
	  $total_sales = 0;

	  foreach($receipts as $receipt){
	     $list_of_close_servers[] = $receipt->server; 
	     $list_of_open_servers[] = $receipt->server_open; 
	     $total_discount += $receipt->discount;

	     if ($receipt->area_category != "bar") {
	       if(preg_match('/vip/', strtolower($receipt->name))){
	         $list_of_others[] = $receipt->name;
	       }else{
	         $list_of_foods[] = $receipt->name;
	       }
	         $total_food_sale += $receipt->count * $receipt->value;
	     }else{
	       $list_of_beverages[] = $receipt->name;
               $total_beverage_sale += $receipt->count * $receipt->value;
	     }
	     $total_sales += $receipt->count * $receipt->value;

	     if(!in_array($receipt->or_no, $receipt_order)){
	      $receipt_order[] = $receipt->or_no;
	      $num_guest += $receipt->number_of_guest;
	     }

	     $number_of_guest = $num_guest;
	  }

	  

	  $highest_close_server_count = array_count_values(array_filter($list_of_close_servers));
	  if(!empty(array_filter($list_of_close_servers))){
	    $highest_close_server = array_search(max($highest_close_server_count), $highest_close_server_count);
	  }else{
	    $highest_close_server = "None";
	  }

	  $highest_open_server_count = array_count_values(array_filter($list_of_open_servers));
	  if(!empty(array_filter($list_of_open_servers))){
	    $highest_open_server = array_search(max($highest_open_server_count), $highest_open_server_count);
	  }else{
	    $highest_open_server = "None";
	  }

	  $this->view_data['from'] = $data["from"] = $from;
	  $this->view_data['to'] = $data["to"] =  $to;

	  $this->view_data['highest_close_server'] = $data["highest_close_server"] =  $this->sanitize($highest_close_server);
	  $this->view_data['highest_open_server'] = $data["highest_open_server"]= $this->sanitize_with_number($highest_open_server);
	  $this->view_data['total_number_of_guest'] = $data["total_number_of_guest"] = $number_of_guest;
	  $this->view_data['total_beverage_sale'] = $data["total_beverage_sale"] = $total_beverage_sale;
	  $this->view_data['total_discount'] = $data["total_discount"] = $total_discount;
	  $this->view_data['total_food_sale'] = $data["total_food_sale"] = $total_food_sale;
	  $this->view_data['total_sales'] = $data["total_sales"] = $total_sales - $total_discount;
	  $this->view_data['total_sale'] = $data["total_sale"] = $total_sale;
	  $this->view_data['total_disc'] = $data["total_disc"] = $total_disc;
	  $this->view_data['top_foods'] = $data["top_foods"] = $this->gettop( $from, $to, "kitchen" );
	  $this->view_data['top_beverages'] = $data["top_beverages"] = $this->gettop( $from, $to, "bar" );
          $this->view_data['json'] = json_encode($data);

          $this->output->set_content_type('application/json');
          $this->output->set_output(json_encode($data));

	
	}

	private function gettop( $from, $to, $type = "bar" )
	{
	  $counter = array();
	  $receipts = $this->db->get_where('receipts', array('created_at >= ' => $from, 'created_at <= ' => $to, 'paid' => 1));
	  
	  foreach( $receipts->result() as $receipt ){
	     $orders = $this->db->get_where('orders', array('receipt_id' => $receipt->id));
	     foreach( $orders->result() as $order ){
	      $item = $this->db->get_where('items', array('id' => $order->item_id));
              $item_name = $this->sanitize($item->row()->name);
	      if( $item->row()->area_category == $type ){
	        if( !isset($counter[$item_name]) ){
	           $counter[$item_name] = 0;
		}

	        if( isset($counter[$item_name]) ){
	           $counter[$item_name] += $order->count;
		}
		
	      }
	     }
	  } 

	 arsort($counter);
	 $counter = array_slice($counter, 0, 10);
	 return $counter;
	}

        function sanitize($string){
            $string = str_replace(array('[\', \']'), '', $string);
            $string = preg_replace('/\[.*\]/U', '', $string);
            $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
            $string = htmlentities($string, ENT_COMPAT, 'utf-8');
            $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
            $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
            return trim($string, '-');
        }

        function sanitize_with_number($string){
            $string = str_replace(array('[\', \']'), '', $string);
            $string = preg_replace('/\[.*\]/U', '', $string);
            $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
            $string = htmlentities($string, ENT_COMPAT, 'utf-8');
            $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
            $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
            $string = preg_replace("/[0-9]/", "", $string);
            return trim($string, '-');
        }


	
}

