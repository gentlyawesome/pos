<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model(array('M_users','M_work_time'));
	}

	public function index(){
	  $config['where']['remove'] = 0;
	  $config['single'] = false;
	  $this->view_data['users'] = $this->M_users->get_record(false, $config);

	  $configr['where']['remove'] = 1;
	  $configr['single'] = false;
	  $this->view_data['users_removed'] = $this->M_users->get_record(false, $configr);
	}
	
	public function create(){
	 if($this->input->post()){
	   $data["name"] = $this->db->escape_str($this->input->post('name'));
	   $data["username"] = $this->db->escape_str($this->input->post('username'));
	   $data["password"] = $this->db->escape_str($this->input->post('password'));
	   $data["usertype"] = $this->db->escape_str($this->input->post('usertype'));

	   if($this->M_users->create($data)){
	     redirect('users');
	   }

	 }
	}

	public function view($id){
	  $config['where']['id'] = $id;
	  $config['single'] = true;
	  $this->view_data['user'] = $user = $this->M_users->get_record(false, $config);

	 if($this->input->post()){
	   $data["name"] = $this->db->escape_str($this->input->post('name'));
	   $data["username"] = $this->db->escape_str($this->input->post('username'));
	   $data["password"] = $this->db->escape_str($this->input->post('password'));
	   $data["usertype"] = $this->db->escape_str($this->input->post('usertype'));

	   if($this->M_users->update($id, $data)){
	     redirect('users');
	   }

	 }
	}

	public function remove($id){

	  $data["remove"] = 1;
	  $this->M_users->update($id, $data);

	  redirect('users');

	}

	public function number_of_hours($username){
	  $config['where']['username'] = $username;
	  $config['single'] = true;
   
	  $this->view_data['user'] = $user = $this->M_users->get_record(false, $config);

	  $ftime = date('Y-m-d 00:00:00');
	  $etime = date('Y-m-d 23:59:59');
	  $query = "select *
	  	    from work_time
		    where username = '".$username."'
		    and created_at >= '".$ftime."'
		    and created_at <= '".$etime."'";
	  $result = $this->db->query($query);

	  $this->view_data['work_time'] = $result->result();

          $query_break_in = $this->db->get_where('work_time', array('username' => $username,
                                 "created_at >=" => $ftime, "created_at <=" => $etime, 'category' => "break_in"));
          $query_break_out = $this->db->get_where('work_time', array('username' => $username,
                                 "created_at >=" => $ftime, "created_at <=" => $etime, 'category' => "break_out"));
    
          $breaks = array();
          foreach($query_break_in->result() as $qbi){
            foreach($query_break_out->result() as $qbo){
		$date1 = new DateTime($qbi->created_at);
		$date2 = new DateTime($qbo->created_at);
		$interval = $date1->diff($date2);
              $breaks["total_hour_" . $qbo->id] =  $interval->i." minutes"; 
            }
          }
         
        $this->view_data["break_ins"] = $query_break_in; 
        $this->view_data['breaks'] = $breaks;
	}

	public function number_of_hours_all(){
	  $config_users['single'] = FALSE;
   
	  $this->view_data['users_list'] = $this->M_users->get_record(false, $config_users);

          if($_POST){
	  $user = $this->input->post('user');
	  $date = $this->input->post('date');
	    if($user == "All"){
	    $config['single'] = FALSE;
   
	    $this->view_data['users'] = $users = $this->M_users->get_record(false, $config);

	    $ftime = date($date.' 00:00:00');
	    $etime = date($date.' 23:59:59');

              foreach($users as $u){
	      $query = "select *
	      	    from work_time
	                where username = '".$u->username."'
	                and created_at >= '".$ftime."'
	                and created_at <= '".$etime."'";
	      $result = $this->db->query($query);

	      $this->view_data['work_time'][$u->id] = $result->result();
	      }

	    }else{
	    $config['where']['id'] = $user;
	    $config['single'] = TRUE;
   
	    $this->view_data['user'] = $user = $this->M_users->get_record(false, $config);

	    $ftime = date($date.' 00:00:00');
	    $etime = date($date.' 23:59:59');

	      $query = "select *
	      	    from work_time
	                where username = '".$user->username."'
	                and created_at >= '".$ftime."'
	                and created_at <= '".$etime."'";
	      $result = $this->db->query($query);

	      $this->view_data['work_time'][$user->id] = $result->result();
            }

          }

	}

	public function number_of_hours_by_date($username, $date){
	  $config['where']['username'] = $username;
	  $config['single'] = true;
   

	  $this->view_data['user'] = $user = $this->M_users->get_record(false, $config);
          $ftime = date('Y-m-d 00:00:00',strtotime(str_replace('-', '/', $date)));
          $etime = date('Y-m-d 23:59:59',strtotime(str_replace('-', '/', $date)));
	  $query = "select *
	  	    from work_time
		    where username = '".$username."'
		    and created_at >= '".$ftime."'
		    and created_at <= '".$etime."'";
	  $result = $this->db->query($query);
	
	  //var_dump($this->db->last_query()); die();

	  $this->view_data['work_time'] = $result->result();


          $query_break_in = $this->db->get_where('work_time', array('username' => $username,
                                 "created_at >=" => $ftime, "created_at <=" => $etime, 'category' => "break_in"));
          $query_break_out = $this->db->get_where('work_time', array('username' => $username,
                                 "created_at >=" => $ftime, "created_at <=" => $etime, 'category' => "break_out"));
    
          $breaks = array();
          foreach($query_break_in->result() as $qbi){
            foreach($query_break_out->result() as $qbo){
		$date1 = new DateTime($qbi->created_at);
		$date2 = new DateTime($qbo->created_at);
		$interval = $date1->diff($date2);
              $breaks["total_hour_" . $qbo->id] =  $interval->i." minutes"; 
            }
          }
         
        $this->view_data["break_ins"] = $query_break_in; 
        $this->view_data['breaks'] = $breaks;
	}
}

