<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_items extends MY_Controller {
	
	public function __construct(){
	  parent::__construct();
	  $this->load->model('M_inventory_items', 'M_invoices', 'M_invoice_items');
	  $this->load->helper('url');
	  $this->view_data["userInfo"] = $userInfo =  $this->userInfo;
	}

	public function add_all_items($id)
	{
	    $this->db->from('invoices'); $invoice_query = $this->db->get();
	    $invoice = $invoice_query->row(); 

	    $this->db->from('invoice_items')->where('invoice_id', $id);
	    $invoice_item_query = $this->db->get();
	    $invoice_items = $invoice_item_query->result();

	    foreach($invoice_items as $ii)
	    {
	      if($ii->quantity_added == 0){
	      $data['item_number'] = $ii->item_number;
	      $data['item_description'] = $ii->item_description;  
	      $data['quantity_in_stock'] = $ii->quantity_in_stock;  
	      $data['type_of_item'] = $ii->type_of_item;  
	      $data['minimum_order_quantity'] = $ii->minimum_order_quantity;  
	      $data['invoice_id'] = $id;  
	      $data['invoice_item_id'] = $ii->id;  
	      $data['date_added'] = date("Y-m-d");  
	
	      $this->db->insert('inventory_items', $data);
	     
              $data_update = array('quantity_added' => 1);
              $this->db->where('id', $ii->id)->update('invoice_items', $data_update);
	      }
	    }

              $invoice_update = array('quantity_added' => 1);
              $this->db->where('id', $invoice->id)->update('invoices', $invoice_update);

	   $this->session->set_flashdata('message', 'Successfully added all items to inventory');
	   redirect('invoices/show/'.$id);
	  
	}

	public function add_item($id)
	{
	    $this->db->from('invoice_items')->where('id', $id);
	    $invoice_item_query = $this->db->get();
	    $invoice_items = $invoice_item_query->row();

	      $data['item_number'] = $invoice_items->item_number;
	      $data['item_description'] = $invoice_items->item_description;  
	      $data['quantity_in_stock'] = $invoice_items->quantity_in_stock;  
	      $data['type_of_item'] = $invoice_items->type_of_item;  
	      $data['minimum_order_quantity'] = $invoice_items->minimum_order_quantity;  
	      $data['invoice_id'] = $invoice_items->invoice_id;  
	      $data['invoice_item_id'] = $invoice_items->id;  
	      $data['date_added'] = date("Y-m-d");  
	
	      $this->db->insert('inventory_items', $data);

              $data_update = array('quantity_added' => 1);
              $this->db->where('id', $invoice_items->id)->update('invoice_items', $data_update);

	   $this->session->set_flashdata('message', 'Successfully added item to inventory');
	   redirect('invoices/show/'.$invoice_items->invoice_id);
	}
}

