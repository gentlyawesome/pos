<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_invoices extends MY_Model{
	protected $_table = 'invoices';

	public function __construct()
	{
	  parent::__construct();
	}

	public function check_if_all_items_are_added($id)
	{
	    $this->db->from('invoices');
	    $invoice_query = $this->db->get();
	    $invoice = $invoice_query->row(); 

	    $this->db->from('invoice_items')->where('invoice_id', $id);
	    $invoice_item_query = $this->db->get();
	    $invoice_items = $invoice_item_query->result();

	    $count = 0;

	    foreach($invoice_items as $ii)
	    {
	      if($ii->quantity_added == 0){
	      $data['item_number'] = $ii->item_number;
	      $data['item_description'] = $ii->item_description;  
	      $data['quantity_in_stock'] = $ii->quantity_in_stock;  
	      $data['type_of_item'] = $ii->type_of_item;  
	      $data['minimum_order_quantity'] = $ii->minimum_order_quantity;  
	      $data['invoice_id'] = $id;  
	      $data['invoice_item_id'] = $ii->id;  
	      $data['date_added'] = date("Y-m-d");  

	      $count ++;
	      }
	    }

	    if($count > 0){
	      return TRUE;
            }else{
	      return FALSE;
	    }
	  
	}

}
