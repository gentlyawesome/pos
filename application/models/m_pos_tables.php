<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_pos_tables extends MY_Model{
	protected $_table = 'pos_table';

	public function __construct()
	{
	  parent::__construct();
	}

	public function check_receipt_order($id){
	  $config['where']['pos_table_id'] = $id;
	  $config['where']['created_at >'] = date('Y-m-d 00:00:00');
	  $config['where']['created_at <'] = date('Y-m-d 24:00:00');
	  $config['where']['paid'] = 0;

	  $receipt  = $this->M_receipts->get_record(false, $config); 

	  if($receipt){
	    return $receipt;
	  }else{
	    return FALSE;
	  }
	}		

	public function check_receipt_order_unpaid($id){
	  $config['where']['pos_table_id'] = $id;
	  $config['where']['paid'] = 0;

	  $receipt  = $this->M_receipts->get_record(false, $config); 

	  if($receipt){
	    return $receipt;
	  }else{
	    return FALSE;
	  }
	}		
}
