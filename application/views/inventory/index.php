<div class="page-header">
  <h1> Inventory </h1>
</div>
<br />

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
     <thead>
     <tr>
	<td>Item Number</td>
	<td>Item Description</td>
	<td>Type of Item</td>
	<td>Quantity in Stock</td>
	<td>Minimum Order Quantity</td>
     </tr>
     </thead>
     <? $total_quantity = 0 ?>
     <? foreach($inventory_items as $row) :?>
     <tr>
	<td><?= $row->item_number; ?></td>
	<td><?= $row->item_description; ?></td>
	<td><?= $row->type_of_item; ?></td>
	<td><?= $row->quantity_in_stock; ?></td>
	<td><?= $row->minimum_order_quantity; ?></td>
     </tr>
     <? $total_quantity += $row->quantity_in_stock; ?>
     <? endforeach ;?>
     <tr>
	<td colspan=3>&nbsp;</td>
	<td><?= $total_quantity; ?></td>
	<td colspan=2>&nbsp;</td>
     </tr>
     </table>
  </div>
</div>
