
<div class="well">
	<p>
		Item/ Menu the restaurant sells.
	</p>
</div>
<form action="" method="post" accept-charset="utf-8">
  <div class="form-group">
  Name:<br />
  <input type="text" name="name" value="<?= !empty($item->name) ? $item->name : '' ?>"  />
  </div>
  <div class="form-group">
  Description:<br />
  <input type="text" name="description" value="<?= !empty($item->description) ? $item->description : ''?>" />
  </div>
  <div class="form-group">
  Category:<br />
  <input type="text" name="category" value="<?= !empty($item->category) ? $item->category : ''?>" />
  </div>
  <div class="form-group">
  Department:<br />
   <select name="area_category" class="form-control">
	  <option value="kitchen">Kitchen</option>
	  <option value="bar">Bar</option>
   </select>
  </div>
  <div class="form-group">
  Value:<br />
  <input type="text" name="value" value="<?= !empty($item->value) ? $item->value : '0.0'?>" />
  </div>
  <div class="form-group">
   <input type="submit" value="Update" />
  </div>
</form>
