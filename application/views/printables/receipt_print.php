<style>
@page {
 margin-top: 20px;
 margin-left: 20px;
}
p{margin:2px; padding:2px;}
</style>
<?php
        //header("Content-Type: application/vnd.ms-word");
        //header("Expires: 0");
        //header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
        //header("Content-disposition: attachment; filename=\"receipt.doc\"");
        ?>

<div id="print_reciept">
<div style="text-align:center; font-family: Arial, Helvetica, sans-serif;">
        <div style="border: 0px; padding: 0px; width: 175px; text-align:center;">
        <p style="font-weight: bold;" align="center">
        ANNIE'S GRILL AND RESTAURANT</p>    

        <p style="font-weight: bold; font-size:10px" align="center">
        Dona Toribia Aspiras Rd. San Antonio, Agoo La Union<br />
        0720607-3910
        </p>    
<br />
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="right"><?= $date; ?></td>
  </tr>
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="left">Table#: <?= $receipt->name; ?></td>
  </tr>
</table>
<br />

<? $discounts = 0.0 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; font-weight:normal;">
<tr class="header">
 <th>Description</th>
 <th>Qty</th>
 <th>Price</th>
</tr>
<tr>
 <td colspan=3>--------------------------------------------</td>
</tr>
<? foreach($orders as $o) :?>
<tr>
 <td><?= $o->name; ?></td>
 <td><?= $o->count ; ?></td>
 <td align='right'>&#8369;<?= $o->value; ?> <?= $order_paid[$o->orderid]->discount_percent > 0 ?  "(".$order_paid[$o->orderid]->discount_percent . "%)"  : '' ;?> </td>
 <? $discounts += $order_paid[$o->orderid]->discount_value; ?>
 <? $total_orders += $o->count * $o->value; ?>
<? endforeach; ?>
<tr>
 <td colspan=3>--------------------------------------------</td>
</tr>
<tr>
 <th align='left'>Total</th>
 <th colspan=2 align="right">&#8369;<?= number_format((float)(((($total_orders) + $receipt->open_price) - $discounts) - $receipt->open_discount), 2, '.', ''); ?></th>
</tr>
<tr>
 <td colspan=3>&nbsp;</td>
</tr>
<tr>
 <td align='left'>CASH</td>
 <td colspan=2 align="right">&#8369;<?= $receipt->payment; ?></td>
</tr>
<tr>
 <td align='left'>CHANGE</td>
 <td colspan=2 align="right">&#8369;<?= $receipt->change + $receipt->open_discount; ?></td>
</tr>
</table>
<br />
<span style="font-size:8px">Thank you for dining with us!</span>
  </div>
</div>

<br />
<!-- if you want a button to print disable comments -->
<!--<input type="button" onclick="printDiv('print_reciept')" value="Print " /> -->
<script type="text/javascript">
//function printDiv(divName) {
    //var printContents = document.getElementById(divName).innerHTML;
    var printContents = document.getElementById('print_reciept').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
//}
</script>
