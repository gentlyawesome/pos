<style>
@page {
 margin-top: 20px;
 margin-left: 20px;
}
p{margin:2px; padding:2px;}
</style>

<div id="print_reciept">
<div style="text-align:center; font-family: Arial, Helvetica, sans-serif;">
        <div style="border: 0px; padding: 0px; width: 175px; text-align:center;">
        <p style="font-weight: bold;" align="center">
        ANNIE'S GRILL AND RESTAURANT</p>    

        <p style="font-weight: bold; font-size:10px" align="center">
        Dona Toribia Aspiras Rd. San Antonio, Agoo La Union<br />
        0720607-3910
        </p>    
<br />
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="right"><?= $date; ?></td>
  </tr>
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="left">Table#: <?= $receipt->name; ?></td>
  </tr>
</table>
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; font-weight:normal;">
<tr class="header">
 <th>Name</th>
 <th>Description</th>
 <th>Qty</th>
</tr>
<tr>
 <td colspan=3>--------------------------------------------</td>
</tr>
<? foreach($orders as $o) :?>
<? if($area_category == $o->area_category) :?>
<tr>
 <td><?= $o->name; ?></td>
 <td><?= $o->description ; ?></td>
 <td><?= $o->count ; ?></td>
<? endif; ?>
<? endforeach; ?>
<tr>
 <td colspan=3>--------------------------------------------</td>
</tr>
</table>
<br />
<span style="font-size:8px">Thank you for dining with us!</span>
  </div>
  </div>

<script type="text/javascript">
//function printDiv(divName) {
    //var printContents = document.getElementById(divName).innerHTML;
    var printContents = document.getElementById('print_reciept').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
//}
</script>
