<style>
@page {
 margin-top: 20px;
 margin-left: 20px;
}
p{margin:2px; padding:2px;}
</style>

<div id="print_reciept">
<div style="text-align:center; font-family: Arial, Helvetica, sans-serif;">
        <div style="border: 0px; padding: 0px; width: 300px; text-align:center;">
        <p style="font-weight: bold;" align="center">
        ANNIE'S GRILL AND RESTAURANT</p>    

        <p style="font-weight: bold; font-size:10px" align="center">
        Dona Toribia Aspiras Rd. San Antonio, Agoo La Union<br />
        0720607-3910
        </p>    
<br />
          <table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="right"><?= $date; ?></td>
  </tr>
  <tr>
    <td colspan=2 style="font-size: 12px; font-family: Arial, Helvetica, sans-serif;" align="left">Date: <?= $from; ?> - <?= $to ?></td>
  </tr>
</table>
<br />

<table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-size:12px; font-family: Arial, Helvetica, sans-serif; font-weight:normal;">
   <tr class="info">
    <td align="left" colspan=4>Food Sales</td>
    <td align="right">&#8369;<?= number_format((float)$total_food_sale, '2','.',''); ?></td>
   </tr>

   <tr class="info">
    <td align="left" colspan=4>Beverage Sales</td>
    <td align="right">&#8369;<?= number_format((float)$total_beverage_sale, '2','.',''); ?></td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Total Discount</td>
    <td align="right">&#8369;<?= number_format((float)$total_disc, '2','.',''); ?></td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Total Sale</td>
    <td align="right">&#8369;<?= number_format((float)$total_sale, '2','.',''); ?></td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Highest Selling Food</td>
    <td align="left">
      <? foreach(array_keys($highest_selling_food) as $fc) :?>
        <?= $fc ;?><br />
      <? endforeach; ?>
    </td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Highest Selling Beverage</td>
    <td align="left">
      <? foreach(array_keys($highest_selling_beverage) as $fc) :?>
        <?= $fc ;?><br />
      <? endforeach; ?>
    </td>
   </tr>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Highest Other Sales</td>
    <td align="right"><?= !empty($highest_other) ? $highest_other : '' ?></td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Server with Highest Sales</td>
    <td align="right"><?= !empty($highest_server) ? $highest_server : '' ?></td>
   </tr>
   <tr class="info">
    <td align="left" colspan=4>Total Number of Guest</td>
    <td align="right"><?= $total_number_of_guest ?></td>
   </tr>
</table>
<br />
  </div>
  </div>

<script type="text/javascript">
//function printDiv(divName) {
    //var printContents = document.getElementById(divName).innerHTML;
    var printContents = document.getElementById('print_reciept').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
//}
</script>
