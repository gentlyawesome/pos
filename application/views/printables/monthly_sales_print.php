
<style>
@page {
 margin-top: 20px;
 margin-left: 10px;
}
p{margin:2px; padding:2px;}
</style>

<div style="text-align:center; font-family: Arial, Helvetica, sans-serif;">
<div class="col-md-12">
<div class="page-header">
  <h1>Monthly Sale <small> From: <?= $date1 ?> - To: <?= $date2 ?></small></h1>
</div>
<table width="100%" border="1" cellspacing="0" cellpadding="0" >
 <thead>
   <tr>
     <th>#</th>
     <th>OR #</th>
     <th class="text-right">Discount</th>
     <th class="text-right">Total</th>
   </tr>
 </thead>
 <? $num = 1; ?>
 <? $total = 0.0; ?>
 <? $discount = 0.0; ?>
 <? if(!empty($paid_items)) :?>
 <? foreach($paid_items as $r) :?>
   <tr>
     <td><?= $num ++; ?></td>
     <td><?= $r->or_no; ?></td>
     <td class="text-right">&#8369;<?= $r->discount_value; ?></td>
     <td class="text-right">&#8369;<?= number_format((float)$r->count * $r->value - $r->discount_value, '2','.',''); ?></td>
   </tr><? $total += $r->count * $r->value - $r->discount_value; ?>
   <? $discount +=  $r->discount_value; ?>
 <? endforeach ;?>
   <tr class="info">
    <td class="text-left" colspan=2>Total</td>
    <td class="text-right">&#8369;<?= number_format((float)$discount, '2','.',''); ?></td>
    <td class="text-right">&#8369;<?= number_format((float)$total, '2','.',''); ?></td>
   </tr>
 <? endif ;?>
</table>
</div>
</div>
