<div style="width:500px; margin:0 auto;">
 <table border=1 style="border-collapse:collapse;" width=500>
  <tr><th colspan=2 align="center">Menu Items</th></tr>
  <tr>
    <th>Item</th>
    <th>Item Sold</th>
  </tr>
 <? $num = 1; ?>
 <? $total = 0.0; ?>
 <? if(!empty($item_categories)) :?>
 <? $counter = 0; ?>
 <? foreach($item_categories as $ic) :?>
 <? $r = $item[$ic->name][0]; ?>
 <? if($r->amount_sold > 0) :?>
   <tr>
     <td><?= $r->name; ?></td>
     <td align="right"><?= $r->amount_sold; ?></td>
   </tr>
 <? endif ;?>
 <? $counter ++; ?>
 <? endforeach ;?>
 <? endif ;?>
 </table>
</div>
<br />
