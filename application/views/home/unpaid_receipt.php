
<? if($area) :?>
  <? $area_batch = array_chunk($area, 3) ?>
  <?# foreach($area_batch as $area) :?>
  <div class="row-fluid">
   <? foreach($area as $a) :?>
     <div class="col-md-4">
      <div class="panel panel-info">
        <!-- Default panel contents -->
        <div class="panel-heading text-center"><?= $a->name ;?></div>
      
	<ul class="list-group">
	<? if($table[$a->id]) :?>
          <? foreach($table[$a->id] as $t) :?>
            <li class="list-group-item">
	    <p>
		<div class="row">
		<div class="col-md-6">
  		  <?= strtoupper($t->name); ?>
		</div>
		<div class="col-md-6 text-right">
	          <a href="<?= site_url();?>receipts/create/<?= $a->id; ?>/<?= $t->id; ?>" class="btn btn-xs btn-success server "><span class="glyphicon glyphicon-plus-sign"></span> Add Order</a> 
		</div>
		</div>
	    </p>
		<? if(!empty($table_receipt_group[$t->id])) :?>
		  <? foreach($table_receipt_group[$t->id] as $receipt) :?>
		    <? if(!empty($receipt)) :?>
		      <? if(!$receipt->remove) :?>
	              <div class="well well-sm text-center">
	<a href="#" class="btn btn-info btn-xs" role="button"><span class="glyphicon glyphicon-time"></span> <?= date("g:ia", strtotime($receipt->created_at)); ?> |  <span class="glyphicon glyphicon-user"></span> <?= urldecode(strtoupper($receipt->server_open)); ?> | <?= date("Y-m-s", strtotime($receipt->created_at)); ?> </a><br />
	<a href="<?= site_url(); ?>receipts/view/<?= $receipt->id; ?>" class="btn btn-info btn-xs" role="button"><span class="glyphicon glyphicon-eye-open"></span> View Order</a>
	<a href="<?= site_url(); ?>receipts/transfer_select_table_category/<?= $receipt->id; ?>" class="btn btn-info btn-xs" role="button"><span class="glyphicon glyphicon-transfer"></span> Transfer Table</a>
	<a href="<?= site_url(); ?>receipts/cancel/<?= $receipt->id; ?>" class="btn btn-danger btn-xs authorize" role="button"><span class="glyphicon glyphicon-minus"></span> Cancel</a>
		     </div>
		     <? endif ;?>
	           <? endif;?><br />
	        <? endforeach; ?>
             <? endif;?>
   	    </li>
          <? endforeach ;?>
        <? endif ;?>
        </ul>
      </div>
     </div>
   <? endforeach ;?>
  </div>
  <?# endforeach ;?>
<? endif ;?>
