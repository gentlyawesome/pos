<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>SimResPOS</title>	
	<script type="text/javascript" charset="utf-8" src="<?= base_url(); ?>assets/js/jquery.js"> </script>
	<script type="text/javascript" charset="utf-8" src="<?= base_url(); ?>assets/js/bootstrap.js"> </script>
	<script type="text/javascript" charset="utf-8" src="<?= base_url(); ?>assets/js/jquery-ui.min.js"> </script>

	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.css"  type="text/css"  media="all" />
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/jquery-ui.min.css"  type="text/css" />
	<!-- <link rel="stylesheet" href="<?#= base_url(); ?>assets/css/font&#45;awesome/font&#45;awesome.css"  type="text/css" /> -->
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/layout.css"  type="text/css" />
</head>
<body>
	<div class="navbar navbar-default">
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="#">SimResPOS</a>
	  </div>
	  <div class="navbar-collapse collapse navbar-responsive-collapse">
	    <ul class="nav navbar-nav">
	    <? if($userInfo['logged_in']) :?>
	      <li><a href='<?= site_url();?>'><span class="glyphicon glyphicon-home"></span> Home</a>
	    </ul>
	   <? endif ;?>
	    <? if($userInfo['logged_in']) :?>
	    <ul class="nav navbar-nav navbar-right">
	      <? if($userInfo['usertype'] == "admin") :?>
	      <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Admin <span class="caret"></span></a>
              <ul class="dropdown-menu">
	      <li><a href='<?= site_url();?>home/unpaid_receipt' class="authorize" ><span class="glyphicon glyphicon-align-justify"></span> Unpaid Receipts</a></li>
	      <li><a href='<?= site_url();?>items'  class="authorize" ><span class="glyphicon glyphicon-eye-open"></span> Items</a></li>
	      <li><a href='<?= site_url();?>receipts/today'  class="authorize"  ><span class="glyphicon glyphicon-calendar"></span> Today's Sale</a></li>
	      <li><a href='<?= site_url();?>receipts/today_sales'  class="authorize"  ><span class="glyphicon glyphicon-calendar"></span> Item Sold Today</a></li>
	      <li><a href='<?= site_url();?>receipts/monthly_sales'  class="authorize"  ><span class="glyphicon glyphicon-calendar"></span> Monthly Sale</a></li>
	      <li><a href='<?= site_url();?>receipts/today_top_sales'  class="authorize"  ><span class="glyphicon glyphicon-calendar"></span> Top Item Sold Today </a></li>
	      <li><a href='<?= site_url();?>users'  class="authorize"  ><span class="glyphicon glyphicon-user"></span> Users/ Employee</a></li>
	      <li><a href='<?= site_url();?>users/number_of_hours_all'  class="authorize"  ><span class="glyphicon glyphicon-user"></span> Employee Timekeeping</a></li>
	      </ul>
              </li>

	      <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-list-alt"></span> Inventory <span class="caret"></span></a>
              <ul class="dropdown-menu">
	      <li><a href='<?= site_url();?>invoices/' class="authorize" ><span class="glyphicon glyphicon-align-justify"></span> Invoice</a></li>
	      <li><a href='<?= site_url();?>inventory/' class="authorize" ><span class="glyphicon glyphicon-align-justify"></span> Inventory</a></li>
	      </ul>
              </li>

	      <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Settings <span class="caret"></span></a>
              <ul class="dropdown-menu">
	      <li><a href='<?= site_url();?>pos_table_categories/' class="authorize" ><span class="glyphicon glyphicon-cutlery"></span> Area</a></li>
	      <li><a href='<?= site_url();?>pos_tables/' class="authorize" ><span class="glyphicon glyphicon-cutlery"></span> Category</a></li>
	      </ul>
              </li>
	    <? endif ;?>
	      <li><a href='<?= site_url();?>auth/logout' class="danger" ><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
	    </ul>
	   <? endif ;?>
	  </div>
	</div>

   <div class="container-fluid">
	<!-- Modal -->
	<div class="modal fade" id="myServer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title text-center" id="myModalLabel">Server</h4>
	      </div>
	      <div class="modal-body">
	      <div class="modal-body-content1">
	      </div>
	      <div class="modal-body-content2">
	      </div>
	      <form action="<?= base_url(); ?>authorization/server" method="post" id="modal-form" /> 
		<p>
			<select name="server" class='form-control'>
			  <? foreach($users as $u) :?>
			    <option value="<?= $u->id ;?>"><?= strtoupper($u->name); ?></option>	
			  <? endforeach ;?>
			</select>
			<input type="password" name="password" placeholder="Password" />
			<input type="hidden" name="current_url" id="current_url" value="<?= current_url(); ?>"  />
			<input type="hidden" name="proccess_url" id="proccess_url"  />
		</p>
		<input type="submit" value="Submit" class="btn btn-success" />
		</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- end Modal -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title text-center" id="myModalLabel">Confirm Authorization</h4>
	      </div>
	      <div class="modal-body">
	      <div class="modal-body-content1">
	      </div>
	      <div class="modal-body-content2">
	      </div>
	      <form action="<?= base_url(); ?>authorization/check" method="post" id="modal-form" /> 
		<p>
			<input type="password" name="username" placeholder="Password" />
			<input type="hidden" name="current_url" id="current_url" value="<?= current_url(); ?>"  />
			<input type="hidden" name="proccess_url" id="proccess_url"  />
		</p>
		<input type="submit" value="Submit" class="btn btn-success" />
		</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- end Modal -->


	<!-- Modal -->
	<div class="modal fade" id="myTime" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title text-center" id="myModalLabel">Time In/ Time Out/ Break in/ Break Out</h4>
	      </div>
	      <div class="modal-body">
	      <div class="modal-body-content1">
	      </div>
	      <div class="modal-body-content2">
	      </div>
	      <form action="<?= base_url(); ?>work_time/time_track" method="post" id="modal-form" /> 
		<p>
			<input type="password" name="username" placeholder="Username/ ID" /><br />
			<input type="text" name="date" placeholder="Date" class="date_select" />
			<input type="hidden" name="current_url" id="current_url" value="<?= current_url(); ?>"  />
			<input type="hidden" name="proccess_url" id="proccess_url"  />
		</p>
		<p>
			<select name="category" class='form-control'>
			  <option value="time_in">Time In</option>
			  <option value="time_out">Time Out</option>
			  <option value="break_in">Break In</option>
			  <option value="break_out">Break Out</option>	
			  <option value="view">View</option>	
			</select>
		</p>
		<input type="submit" value="Submit" class="btn btn-success" />
		</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- end Modal -->

	<? if($this->session->flashdata('msg')) :?>
	<div class="row">
	 <div class="col-md-12 text-center">
	  <?= $this->session->flashdata('msg'); ?>
	 </div>
	</div>
	<? endif ;?>

	<?= $yield ?>
   </div>

<script type="text/javascript" charset="utf-8">
 $(document).ready(function(){
  $('input[type=text]').addClass('form-control');
  $('input[type=hidden]').removeClass('form-control');
  $('input[type=password]').addClass('form-control');
  $('input[type=submit]').addClass('btn btn-success btn-block btn-lg');
	$('.server').click(function(e){
	  e.preventDefault();
	  $('#myServer').modal('show');
	  $('#myServer #proccess_url').val($(this).attr('href'));
	});

	$('.authorize').click(function(e){
	  e.preventDefault();
	  $('#myModal').modal('show');
	  $('#myModal #proccess_url').val($(this).attr('href'));
	});


	$('.work_time').click(function(e){
	  e.preventDefault();
	  $('#myTime').modal('show');
	  $('#myTime #proccess_url').val($(this).attr('href'));
	});

	$('#modal-form').submit(function(){
	  $('#myModal').modal('hide');
	});
        $('.date_select').datepicker();
        $('.date_select').datepicker("option", "dateFormat", "yy-mm-dd");
 });
</script>
</body>
</html>
