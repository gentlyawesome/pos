<? if(!empty($this->session->flashdata('message'))) :?>
<div class="row">
  <div class="col-md-12">
  <div class="alert alert-success">
    <?= $this->session->flashdata('message') ?>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>
  </div>
</div>
<? endif ;?>

<div class="page-header">
  <h1>Add Invoice</h1>
</div>

<?= form_open('invoices/add') ?>
<div class="row">
  <div class="col-md-12">
    <?= validation_errors(); ?>
  </div>
</div>

<div class="row">
 <div class="col-md-4">
   <label for='vendor_id'>Vendor ID</label>
   <input type='input' name='vendor_id' class="form-control" placeholder='Vendor ID' /><br />
 </div>
 <div class="col-md-4">
   <label for='invoice_number'>Invoice Number</label>
   <input type='input' name='invoice_number' class="form-control" placeholder='Invoice Number' /><br />
 </div>
 <div class="col-md-4">
   <label for='invoice_date'>Invoice Date</label>
   <input type='input' name='invoice_date' class="form-control date_select" placeholder='Invoice Date' /><br />
 </div>
</div>


<div class="page-header">
  <h1>Invoice Items</h1>
</div>

<? for($i=1; $i < 10; $i++) : ?>
<div class="row">
 <div class="col-md-2">
   <label for='item_number'>Item Number</label>
   <input type='input' name='item_number_<?= $i ?>' class="form-control" placeholder='Item Number' /><br />
 </div>
 <div class="col-md-3">
   <label for='item_description'>Item Description</label>
   <input type='input' name='item_description_<?= $i ?>' class="form-control" placeholder='Item Description' /><br />
 </div>
 <div class="col-md-2">
   <label for='quantity_in_stock'>Quantity Stock</label>
   <input type='input' name='quantity_in_stock_<?= $i ?>' class="form-control " placeholder='0' /><br />
 </div>
 <div class="col-md-3">
   <label for='type_of_item'>Type of Item</label>
   <input type='input' name='type_of_item_<?= $i ?>' class="form-control " placeholder='Type of Item' /><br />
 </div>
 <div class="col-md-2">
   <label for='minimum_order_quantity'>Minimum Order Quantity</label>
   <input type='input' name='minimum_order_quantity_<?= $i ?>' class="form-control " placeholder='0' /><br />
 </div>
</div>
<? endfor ; ?>

<div class="row">
  <div class="col-md-12">
    <input type="submit" name="submit" value="Add Invoice" class="btn btn-block btn-success" />
  </div>
</div>
</form>
<br />

<script type="text/javascript">
$('.date_select').datepicker();
$('.date_select').datepicker("option", "dateFormat", "yy-mm-dd");
</script>
