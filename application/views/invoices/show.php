<? if(!empty($this->session->flashdata('message'))) :?>
<div class="row">
  <div class="col-md-12">
  <div class="alert alert-success">
    <?= $this->session->flashdata('message') ?>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>
  </div>
</div>
<? endif ;?>

<div class="page-header">
  <h1>Show Invoice</h1>
  <small>
    <a href="<?= site_url(); ?>invoices" class="btn btn-primary"> Invoices</a>
    <a href="<?= site_url(); ?>inventory_items/add_all_items/<?= $invoice->id; ?>" class="btn btn-success"> Add all items to inventory</a>
    <a href="<?= site_url(); ?>invoices/revert_items/<?= $invoice->id; ?>" class="btn btn-success"> Revert items</a>
  </small>
</div>

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
     <thead>
     <tr>
	<td>Vendor ID</td>
	<td>Invoice Number</td>
	<td>Date</td>
     </tr>
     </thead>
     <tr>
	<td><?= $invoice->vendor_id; ?></td>
	<td><?= $invoice->invoice_number; ?></td>
	<td><?= date("Y-m-d", strtotime($invoice->invoice_date)); ?></td>
     </tr>
     </table>
    <br />
    <table class="table table-bordered table-striped">
     <thead>
     <tr>
	<td>Item Number</td>
	<td>Item Description</td>
	<td>Type of Item</td>
	<td>Quantity in Stock</td>
	<td>Minimum Order Quantity</td>
	<td>Status</td>
     </tr>
     </thead>
     <? $total_quantity = 0 ?>
     <? foreach($invoice_items as $row) :?>
     <tr>
	<td><?= $row->item_number; ?></td>
	<td><?= $row->item_description; ?></td>
	<td><?= $row->type_of_item; ?></td>
	<td><?= $row->quantity_in_stock; ?></td>
	<td><?= $row->minimum_order_quantity; ?></td>
	<? if($row->quantity_added) :?>
	 <td class="success text-center">Item is added to inventory
<a href="<?= site_url(); ?>invoices/revert_item/<?= $row->id ?>" class="btn btn-primary"> Revert </a>
         </td>
	<? else :?>
         <td><a href="<?= site_url(); ?>inventory_items/add_item/<?= $row->id ?>" class="btn btn-success"> Add to inventory </a></td>
	<? endif ;?>
     </tr>
     <? $total_quantity += $row->quantity_in_stock; ?>
     <? endforeach ;?>
     <tr>
	<td colspan=3>&nbsp;</td>
	<td><?= $total_quantity; ?></td>
	<td colspan=2>&nbsp;</td>
     </tr>
     </table>
  </div>
</div>

