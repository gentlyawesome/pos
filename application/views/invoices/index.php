<? if(!empty($this->session->flashdata('message'))) :?>
<div class="row">
  <div class="col-md-12">
  <div class="alert alert-success">
    <?= $this->session->flashdata('message') ?>
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  </div>
  </div>
</div>
<? endif ;?>

<div class="page-header">
 <h1> Invoice Index </h2><small><a href="<?= site_url() ;?>invoices/add" class="btn btn-success"> Add Invoice </a></small>
</div>

<div class="row">
<div class="col-md-12">
<form action="" method="post" accept-charset="utf-8" class="form-inline" >
  <div class="col-md-4">
  <input type="text" name="from" id="from"  class="input-lg date_select" placeholder="from" />
  </div>

  <div class="col-md-4">
  <input type="text" name="to" id="to"  class="input-lg date_select" placeholder="to" />
  </div>

  <div class="col-md-4">
  <button type="submit" class="btn btn-success btn-block btn-lg"  ><span class="glyphicon glyphicon-ok-circle" ></span> <b>Generate Report</b></button>
  </div>

</form>
</div>
</div>

<br />
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
     <thead>
     <tr>
	<td>Vendor ID</td>
	<td>Invoice Number</td>
	<td>View</td>
     </tr>
     </thead>
     <? if(!empty($invoices)) :?>
     <? foreach($invoices->result() as $row) :?>
     <tr>
	<td><?= $row->vendor_id ?></td>
	<td><?= $row->invoice_number ?></td>
	<td>
          <a href='<?= site_url(); ?>invoices/show/<?= $row->id; ?>' class="btn btn-success">View Invoice</a>
          <a href='<?= site_url(); ?>invoices/delete/<?= $row->id; ?>/<?= $from; ?>/<?= $to; ?>' class="btn btn-danger" onClick="return doconfirm();">Delete</a>
        </td>
     </tr>
     <? endforeach ;?>
     <? else :?>
     <tr>
	<td colspan=3 class="text-center">There is no data found</td>
     </tr>
     <? endif ;?>
    </table>
  </div>
</div>

<script type="text/javascript">
$('.date_select').datepicker();
$('.date_select').datepicker("option", "dateFormat", "yy-mm-dd");

function doconfirm()
{
    job=confirm("Are you sure to delete permanently?");
    if(job!=true)
    {
        return false;
    }
}
</script>
