
<div class="row">
<div class="col-md-12">
<div class="page-header">
  <h1>Today's Sale<small>list of top items sold</small></h1>
</div>
<table class="table table-bordered">
 <thead>
   <tr>
     <th>#</th>
     <th>Category</th>
     <th>Name</th>
     <th>Description</th>
     <th># of Orders</th>
   </tr>
 </thead>
 <? $num = 1; ?>
 <? $total = 0.0; ?>
 <? if(!empty($item_categories)) :?>
 <? $counter = 0; ?>
 <? foreach($item_categories as $ic) :?>
 <? $r = $item[$ic->category][0]; ?>
 <? if($r->amount_sold > 0) :?>
   <tr>
     <td><?= $num ++; ?></td>
     <td><?= $r->category; ?></td>
     <td><?= $r->name; ?></td>
     <td><?= $r->description; ?></td>
     <td><?= $r->amount_sold; ?></td>
   </tr>
 <? endif ;?>
 <? $counter ++; ?>
 <? endforeach ;?>
 <? endif ;?>
</table>
</div>
</div>
