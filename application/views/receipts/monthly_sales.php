<form action="" method="post" accept-charset="utf-8">
  <div class="form-group">
   <input type="text" name="date1" placeholder="From" class="datepicker" />
   <input type="text" name="date2" placeholder="To" class="datepicker" />
  </div>
  <div class="form-group">
   <input type="submit" value="Submit" />
  </div>
</form>

<? if(!empty($date1) and !empty($date2)) :?>
<div class="row">
<div class="col-md-12">
<div class="page-header">
  <h1>Monthly Sale <small> From: <?= $date1 ?> - To: <?= $date2 ?></small></h1>
</div>
<table class="table table-bordered">
 <thead>
   <tr>
     <th>#</th>
     <th>OR #</th>
     <th class="text-right">Discount</th>
     <th class="text-right">Total</th>
   </tr>
 </thead>
 <? $num = 1; ?>
 <? $total = 0.0; ?>
 <? $discount = 0.0; ?>
 <? if(!empty($paid_items)) :?>
 <? foreach($paid_items as $r) :?>
   <tr>
     <td><?= $num ++; ?></td>
     <td><?= $r->or_no; ?></td>
     <td class="text-right">&#8369;<?= $r->discount_value; ?></td>
     <td class="text-right">&#8369;<?= number_format((float)$r->count * $r->value - $r->discount_value, '2','.',''); ?></td>
   </tr><? $total += $r->count * $r->value - $r->discount_value; ?>
   <? $discount +=  $r->discount_value; ?>
 <? endforeach ;?>
   <tr class="info">
    <td class="text-left" colspan=2>Total</td>
    <td class="text-right">&#8369;<?= number_format((float)$discount, '2','.',''); ?></td>
    <td class="text-right">&#8369;<?= number_format((float)$total, '2','.',''); ?></td>
   </tr>
   <tr >
    <td colspan=4 ><a href="<?= base_url(); ?>receipts/monthly_sales_print/<?= $date1?>/<?= $date2 ?>" class="btn btn-block btn-success">Print</a></td></tr>
   </tr>
 <? endif ;?>
</table>
</div>
</div>
<? endif ;?>

<script type="text/javascript" charset="utf-8">
$('.datepicker').datepicker();
$('.datepicker').datepicker("option", "dateFormat", "yy-mm-dd");
</script>
