
<div class="row">
<div class="col-md-12">
<div class="page-header">
  <h1>Today's Sale<small>list of items sold</small></h1>
</div>
<table class="table table-bordered">
 <thead>
   <tr>
     <th>#</th>
     <th>Name</th>
     <th>Description</th>
     <th># of Orders</th>
     <th class="text-right">Discount</th>
     <th class="text-right">Total</th>
   </tr>
 </thead>
 <? $num = 1; ?>
 <? $total = 0.0; ?>
 <? $discount = 0.0; ?>
 <? if(!empty($paid_items)) :?>
 <? foreach($paid_items as $r) :?>
   <? $discount +=  ($r->discount_value + $r->open_discount); ?>
   <tr>
     <td><?= $num ++; ?></td>
     <td><?= $r->name; ?></td>
     <td><?= $r->description; ?></td>
     <td><?= $r->count; ?></td>
     <td class="text-right">&#8369;<?= ($r->discount_value - $r->open_discount); ?></td>
     <td class="text-right">&#8369;<?= number_format((float)$r->count * $r->value + $discount, '2','.',''); ?></td>
   </tr><? $total += $r->count * $r->value + $discount; ?>
 <? endforeach ;?>
   <tr class="info">
    <td class="text-left" colspan=4>Total</td>
    <td class="text-right">&#8369;<?= number_format((float)$discount, '2','.',''); ?></td>
    <td class="text-right">&#8369;<?= number_format((float)$total, '2','.',''); ?></td>
   </tr>
 <? endif ;?>
</table>
</div>
</div>
