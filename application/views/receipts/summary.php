<div class="row">
  <div class="col-md-12">
    <div class="page-header">
      <h1>Summary</h1>
    </div>
  </div>
</div>

<div class="row">
<div class="col-md-6">
<? if(!empty($from) && !empty($to)) : ?>
 <h2><?= $from ?> - <?= $to ?></h2>
<? else :?>
 <h2> Please select the date </h2>
<? endif ?>
</div>

<div class="col-md-6 ">
<p>
<form action="" method="post" accept-charset="utf-8" class="form-inline text-center"  >
  <input type="text" name="from" id="from"  class="date_select" placeholder="from" />
  <input type="text" name="to" id="to"  class="date_select" placeholder="to" />
  <button type="submit" class="btn btn-success  "  ><span class="glyphicon glyphicon-ok-circle" ></span> <b>Generate Report</b></button>
</form>
</p>
</div>
</div>


<div class="row" ng-app="sampleApp" ng-controller="SampleCtrl">
<div class="col-md-12">
<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-info text-center"><h3>Summary Result</h3></div>
  <!-- Table -->
<? if(!empty($receipts)) :?>
<table class="table">
   <tr class="info">
    <td class="text-left" colspan=4><h4><h4>Food Sales</h4></td>
    <td class="text-right"><h3>&#8369;<?= number_format((float)$total_food_sale, '2','.',''); ?></h3></td>
   </tr>

   <tr class="info">
    <td class="text-left" colspan=4><h4>Beverage Sales</h4></td>
    <td class="text-right"><h3>&#8369;<?= number_format((float)$total_beverage_sale, '2','.',''); ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Total Discount</h4></td>
    <td class="text-right"><h3>&#8369;<?= number_format((float)$total_disc, '2','.',''); ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Total Sale</h4></td>
    <td class="text-right"><h3>&#8369;<?= number_format((float)$total_sale, '2','.',''); ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Highest Selling Food</h4></td>
    <td class="text-left">
    <ul class"list-group">
      <? foreach($top_foods as $index => $key) :?>
        <li class="list-group-item"><?= $index ;?>  <span class="badge"><?= $key ?></span></li>
      <? endforeach; ?>
    </ul>
    </td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Highest Selling Beverage</h4></td>
    <td class="text-left">
    <ul>
      <? foreach($top_beverages as $index => $key) :?>
        <li class="list-group-item"><?= $index ;?>  <span class="badge"><?= $key ?></span></li>
      <? endforeach; ?>
    </ul>
    </td>
   </tr>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Highest Other Sales</h4></td>
    <td class="text-right"><h3><?= !empty($highest_other) ? $highest_other : '' ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Server of the Day</h4></td>
    <td class="text-right"><h3><?= !empty($highest_close_server) ? $highest_close_server : '' ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Cashier of the Day</h4></td>
    <td class="text-right"><h3><?= !empty($highest_open_server) ? $highest_open_server : '' ?></h3></td>
   </tr>
   <tr class="info">
    <td class="text-left" colspan=4><h4>Total Number of Guest</h4></td>
    <td class="text-right"><h3><?= $total_number_of_guest ?></h3></td>
   </tr>
</table>
<? endif; ?>
<? if(!empty($receipts)) :?>
 <a href="<?= base_url(); ?>receipts/summary_print/<?= $from?>/<?= $to ?>" class="btn btn-block btn-success"><h3>Print</h3></a>
 <a ng-click="sendReport('<?= base_url(); ?>summary_report/report/<?= $raw_from?>/<?= $raw_to ?>'); $event.preventDefault()" class="btn btn-block btn-success" id="sendReport"><h3>Send Report</h3></a> 
<? endif ;?>
</div>
<br />
</div>
</div>
<script type="text/javascript">
$('.date_select').datepicker();
$('.date_select').datepicker("option", "dateFormat", "yy-mm-dd");
</script>
<!-- AngularJS -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>

<!-- Firebase -->
<script src="https://cdn.firebase.com/js/client/2.2.4/firebase.js"></script>

<!-- AngularFire -->
<script src="https://cdn.firebase.com/libs/angularfire/1.1.2/angularfire.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<?= base_url(); ?>assets/js/app.js"> </script>
