 <? $total_count = 0; ?>
 <? $total_val = 0; ?>
 <? $total_disc = 0; ?>
 <? $total_pri = 0; ?>
 <? $total_price = 0; ?>
 <? $open_price = 0; ?>
 <? $price_total = 0; ?>
<form action="" method="post" accept-charset="utf-8" id="order_form">
<div class="row">
<div class="col-md-6">
<? if(!$receipt->paid) :?>
<div class="page-header">
  <h1><?= $receipt->name; ?> <small>list of items</small></h1>
</div>

<? if(!empty($items)) :?>
<ul class="nav nav-tabs" id="tabs">
 <? foreach($items as $i) :?>
   <li><a href="#" id="item_<?= $i->id; ?>"><?= strtoupper($item_category[$i->id]) ?></a></li>
 <? endforeach ;?>
</ul>
<? endif ;?>
<br />

 <? if(!empty($items)) :?>
 <? foreach($items as $it) :?>
<div class="table-responsive item_list" id="item_<?= $it->id ?>C" >
<table class="table table-bordered">
 <thead>
   <tr>
     <th>Name</th>
     <th>Description</th>
     <th>Value</th>
     <th>Add</th>
   </tr>
 </thead>
 <? foreach($item[$it->category] as $i) :?>
   <tr>
     <td><?= $i->name; ?></td>
     <td><?= $i->description; ?></td>
     <td>
     <input type="text" name="order_count" class="order_count" placeholder="0" style="width:50px;"  />
     </td>
     <td>&#8369; <?= $i->value; ?></td>
     <td>
     <a href="<?= site_url();?>orders/create/<?= $receipt->pos_table_id; ?>/<?= $i->id; ?>/<?= $receipt->id; ?>" class="btn btn-success btn-block order_link"><span class="glyphicon glyphicon-plus"></span> </a>
   </td>
   </tr>
 <? endforeach ;?>
</table>
</div>
 <? endforeach ;?>
 <? endif ;?>
<? endif;?>
</div>

<? if($receipt->paid) :?>
<? $col = "12"; ?>
<? else :?>
<? $col = "6"; ?>
<? endif ;?>

<div class="col-md-<?= $col; ?>">
<div class="row">
<div class="col-md-12">
<div class="page-header">
  <h1>Orders <small>list of orders</small></h1>
</div>
<div class="table-responsive">
<table class="table table-striped" id="table_receipt">
  <? if($receipt->paid) :?>
  <tr class="success">
    <th>OR # <?= $receipt->or_no ;?> </th>
    <th>Date: <?= date("Y-m-d", strtotime($receipt->created_at)) ;?> </th>
    <th># of Guest: <?= $receipt->number_of_guest ;?> </th>
    <th>Server Recipt Creator: <?= strtoupper($receipt->server_open) ;?> </th>
    <th>Server Receipt Bill: <?= strtoupper($receipt->server) ;?> </th>
  </tr>
  <? endif ;?>
   <tr class="info">
     <th>Name</th>
     <th>#</th>
     <th>Price</th>
     <? if($receipt_paid) :?>
     <th>Discount</th>
     <th class='text-right'>Total</th>
     <? endif ;?>
     <? if(!$receipt->paid) :?>
     <th>% Discount</th>
     <th># Discount</th>
     <th>Total</th>
     <th>Remove</th>
     <? endif ;?>
   </tr>
 <? if(!empty($orders)) :?>
 <? foreach($orders as $o) :?>
<? if(false) :?>
<form action="" method="post" accept-charset="utf-8" id="order<?= $o->id ?>">
<? endif ;?>
<input type='hidden' class='order_id' name="post_name" value="discount">
 <? $total_count += $o->count; ?>
   <tr>
     <td><?= $o->name; ?></td> 
     <td><?= $o->count; ?></td>
     <? if($receipt_paid) :?>
     <? $price_total += $o->value * $o->count; ?>
     <td>&#8369; <?= number_format((float)$o->value * $o->count, '2','.','') ; ?> </td>
     <td>&#8369; <?= number_format((float)$order_paid[$o->orderid]->discount_value, '2','.',''); ?> (<?= $order_paid[$o->orderid]->discount_percent; ?>%)</td>
     <? $total_disc += (float)$order_paid[$o->orderid]->discount_value; ?>
     <? else :?>
     <td>&#8369; <?= floatval($o->value) ; ?></td>
     <? $total_price += floatval($o->value * $o->count) ; ?>
     <? endif; ?>
     <? if($receipt_paid) :?>
     <td class="text-right">&#8369; <?= number_format((float)($o->count * $o->value)- floatval($o->discount_value), '2', '.', ''); ?>
     <? $total_price = (float)($o->count * $o->value)- floatval($o->discount_value); ?>
     <? endif; ?>
     <? $total_val += (float)$total_price; ?>
     </td>
     <? if(!$receipt->paid) :?>
     <td>
     <select name="order_ids[<?= $o->orderid; ?>]" id="order_ids<?= $o->orderid; ?>" class="form-control">
		  <option value="0">0</option>
		  <option value="10">10</option>
		  <option value="20">20</option>
		  <option value="30">30</option>
	   </select><input type='hidden' class='total_order_val' value=<?= number_format((float)$o->value * $o->count, '2','.','') ; ?>>
     </td>
     <td>
     <select name="order_discount_ids[<?= $o->orderid; ?>]" id="order_discount_ids<?= $o->orderid; ?>" class="form-control discount_count">
     <?php for($i=1; $i <= $o->count; $i++) :?>
	 <option value=<?= $i ?>><?= $i ?></option>
     <?php endfor; ?>
     </td>
     <? if(false) :?>
     <td><button type="submit" class="btn btn-success btn-block btn-xs"><b>Add <br />Discount</b></button></td>
     <? endif; ?>
     <td>&#8369; <?= number_format((float)$o->value * $o->count, '2','.','') ; ?></td>
     <td>
     <a href="<?= site_url();?>orders/remove/<?= $o->orderid ?>" class="btn btn-danger btn-xs authorize"><span class="glyphicon glyphicon-minus"></span> </a>
     <a href="<?= site_url();?>orders/void/<?= $o->orderid ?>" class="btn btn-danger btn-xs authorize" data-toggle="tooltip" data-placement="top" title="Void Order"><span class="glyphicon glyphicon-remove-circle"></span> </a>
     </td>
     <? endif; ?>
   </tr>
<? if(false) :?>
</form>
<? endif ;?>
 <? endforeach ;?>
<? if($receipt->paid) :?>
<tr>
<td class='text-left'><b>Open Price</b></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td colspan=2 class="text-right"><b>&#8369; <?= number_format(floatval($receipt->open_price), '2', '.', ''); ?></b> </td>
</tr>
<tr>
<td class='text-left'><b>Open Discount</b></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td colspan=2 class="text-right"><b>&#8369; <?= number_format(floatval($receipt->open_discount), '2', '.', ''); ?></b> </td>
</tr>
</tr>
<? endif ;?>
 <tr>
<td class='text-left'><b>Total</b></td>
<td><?= $total_count; ?></td>
<? if($receipt->paid) :?>
<td>&#8369; <?= number_format((float)$price_total,'2','.',''); ?></td>
<td>&#8369; <?= number_format((float)$total_disc,'2','.',''); ?></td>
<? endif ;?>
<td colspan=2 class="text-right" id="price_total"><b>&#8369; <?= number_format(floatval((($total_price - $total_disc) + $receipt->open_price) - $receipt->open_discount), '2', '.', ''); ?></b> </td></tr>
 <tr>
<td colspan=7 class="text-right"><a href="<?= base_url(); ?>receipts/receipt_print/<?= $receipt->id?>" class="btn  btn-info">Print Receipt</a>
<a href="<?= base_url(); ?>receipts/receipt_order/<?= $receipt->id?>/kitchen" class="btn  btn-info">Print Kitchen</a>
<a href="<?= base_url(); ?>receipts/receipt_order/<?= $receipt->id?>/bar" class="btn  btn-info">Print Bar</a></td></tr>
 <? endif ;?>
</table>
</div>
</div>
</div>


<? if(!$receipt->paid) :?>
<div class="row">
<div class="col-md-12">

<div class="row">
<div class="col-md-6">
<div class="form-group">
<div class="input-group">
Server Opened: <?= strtoupper($receipt->server_open); ?></br>
Server:<br />
<select name="server" class='form-control'>
  <? foreach($users as $u) :?>
    <option value="<?= $u->id ;?>"><?= strtoupper($u->name); ?></option>	
  <? endforeach ;?>
</select>
</div>
</div>
OR#
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>#</span>
<input type="text" name="or_no" id="or_no" placeholder="OR Number" class="input-lg" />
</div>
</div>
Date:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'><span class="glyphicon glyphicon-calendar"></span></span>
<input type="text" name="created_at" id="created_at"  class="input-lg" />
</div>
</div>
Payment:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>&#8369;</span>
<input type="text" name="cash_handed_value" id="cash_handed_value" placeholder="0.00" class="input-lg" />
</div>
</div>
Change:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>&#8369;</span>
<input type="text" name="change" id="change" placeholder="0.00" class="input-lg" />
<input type="hidden" name="change_value" id="change_value"  />
</div>
</div>
</div>
<div class="col-md-6">
Server Password:
<br />
<div class="form-group">
<div class="input-group">
<input type="password" name="serverpass"  />
</div>
</div>
Open Price:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>&#8369;</span>
<input type="text" name="open_price" id="open_price" placeholder="0.0" class="input-lg" />
</div>
</div>
Open Price Remarks:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>R</span>
<input type="text" name="open_price_remarks" id="open_price_remarks" placeholder="Open Price Remarks" class="input-lg" />
</div>
</div>
Discount:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>&#8369;</span>
<input type="text" name="open_discount" id="open_discount" placeholder="0" class="input-lg" />
</div>
</div>
Discount Remarks:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>R</span>
<input type="text" name="discount_remarks" id="discount_remarks" placeholder="Remarks" class="input-lg" />
</div>
</div>
Number of Guest:
<br />
<div class="form-group">
<div class="input-group">
<span class='input-group-addon'>#</span>
<input type="text" name="number_of_guest" id="number_of_guest" placeholder="0" class="input-lg" />
</div>
</div>
</div>
</div>

<br />
</div>
</div>

	<div class="row">
	  <div class="col-md-6">
	  <button type="submit" class="btn btn-success btn-block btn-lg"  id='bill'><span class="glyphicon glyphicon-ok-circle" ></span> <b>Accept Payment</b></button>
	  </div>
	  <div class="col-md-6">
	  <a href="#" class="btn btn-block btn-danger btn-lg clear_cash"><span class="glyphicon glyphicon-ban-circle"></span> <b>Clear</b></a>
	  </div>
        </div><br />	
<? endif;?>
</div>
</div>
</form>

<script type="text/javascript" charset="utf-8">
$('#cash_handed').attr('disabled','disabled');
$('#bill').attr('disabled','disabled');
$('#submit_discount').removeAttr('disabled');
var order_value = 0;
<? $total_order = floatval($total_price + $receipt->open_price)?>
 <? if($orders) :?>
 <? foreach($orders as $o) :?>
   $('#order_ids<?= $o->orderid; ?>').change(function(){
     var total_order_value_discount = 0.0;
     var this_val = $(this).val() / 100; //percentage
     var total_order = <?= $total_order ?>;
     var discount_count = $(this).closest('td').next('td').find('.discount_count').val();
     var discounted_order = parseFloat((discount_count * <?= $o->value ?>) * this_val);
     var order_price = parseFloat(<?= $o->value;?>);
     var original_price = parseFloat(<?= $o->value * $o->count; ?>);
     var discount = parseFloat(order_price * this_val * discount_count); 
     var order_val = parseFloat(original_price - discount); 
     $(this).closest('td').find('.total_order_val').val(parseFloat(order_val));
     $(this).closest('td').next('td').next('td').html('&#8369;' + parseFloat(order_val));

     $('.total_order_val').each(function(){
       total_order_value_discount += parseFloat($(this).val());  
     });

     var total_price = total_order_value_discount;
     order_value = total_price;
     $('#price_total').html("<b>&#8369; " + total_price  +"</b>");
   });
 <? endforeach ;?>
 <? endif ;?>
$('#cash_handed_value').keyup(function(){
 if(order_value == 0){
  order_value = <?= $total_order ;?>;
 }
 payable = parseFloat(order_value);
 current_value = parseFloat($(this).val());
 if(current_value >= payable){
  $('#bill').removeAttr('disabled');
 }else{
  $('#bill').attr('disabled','disabled');
 } 
 //current_value = parseFloat(current_value) + parseFloat($(this).attr('id'));

 //$('#cash_handed').val(parseFloat(current_value));
 //$('#cash_handed_value').val($('#cash_handed'));
 //$('#cash_handed').attr('placeholder', current_value);

 change = (parseFloat(payable)) - parseFloat(current_value);

 if(change < 0){
  change *= -1
 }
 if(current_value > 0 && change >= 0){
   $('#bill').removeAttr('disabled');
   $('#bill').addClass('btn btn-block btn-success');
  }

 $('#change').val(change);
 $('#change_value').val(change);
 $('#change').attr('placeholder', change);

});

$('.clear_cash').click(function(){
 $('#cash_handed_value').val('0.0');
 $('#cash_handed_value').attr('placeholder', '0.00');

 $('#change').val('');
 $('#change_value').val('0.0');
 $('#change').attr('placeholder', '0.00');

  //$('#bill').attr('disabled', 'disabled');
  //$('#bill').removeAttr('class');
  //$('#bill').addClass('btn btn-block btn-default');
});

$('#tabs li:first').addClass('active');
$('.item_list').hide();
$('.item_list:first').show();

$('#tabs li a').click(function(){
  $('#tabs li a').removeClass('active');           
  $('#tabs li').removeClass('active');

    var t = $(this).attr('id');
    $(this).addClass('active');
    $(this).parent('li').addClass('active');
    
    $('.item_list').hide();
    $('#'+ t + 'C').show();
});

//$('.order_count').keyup(function(){
   //if($(this).val().length > 0){
   //var count = $(this).val();
   //var link = $(this).parent().parent().children().find("a");
   //var href = $(this).parent().parent().children().find("a").attr('href');
   //link.attr('href', href + "/" + count);
   //}
//});

$('.order_link').click(function(){
  var count = $(this).closest('tr').find('.order_count').val();
  var href = $(this).attr('href') + "/" + count ;
  $(this).attr('href', href); 
});

$('#discount_form_enable').click(function(){
  $('#order_form').attr('disabled');
});

$('#created_at').datepicker();
$('#created_at').datepicker("option", "dateFormat", "yy-mm-dd");
	
</script>
