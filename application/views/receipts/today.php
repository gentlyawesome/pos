
<div class="row">
<div class="col-md-12">
<div class="page-header">
  <h1>Today's Sale<small> list of paid receipts</small></h1>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
 <h2><?= $today ?></h2>
</div>
<div class="col-md-6 ">
<form action="" method="post" accept-charset="utf-8" class="form-inline text-center"  >
  <input type="text" name="today" id="today"  class="form-control date_select" placeholder="Date" />
  <button type="submit" class="btn btn-success "  ><span class="glyphicon glyphicon-ok-circle" ></span> <b>Generate Report</b></button>
</form>
</div>
</div>

<div class="row">
<div class="col-md-12">
<table class="table table-bordered">
 <thead>
   <tr>
     <th>View</th>
     <th>#</th>
     <th>OR #</th>
     <th>Area</th>
     <th>Table</th>
     <th class="text-right">Total</th>
   </tr>
 </thead>
 <? $num = 1; ?>
 <? $total_val = 0.0; ?>
 <? if(!empty($paid_receipts)) :?>
 <? foreach($paid_receipts as $r) :?>
   <tr>
     <td>
	<a href="<?= site_url(); ?>receipts/view/<?= $r->receiptid; ?>" class="btn btn-sm btn-info btn-block" role="button"><span class="glyphicon glyphicon-eye-open"></span> View Order</a>
     </td>
     <td><?= $num ++; ?></td>
     <td><?= $r->or_no; ?></td>
     <td><?= $r->pos_table_category_name; ?></td>
     <td><?= $r->pos_table_name; ?></td>
     <? $total = $receipt_value[$r->id] ?>
     <td class="text-right">&#8369;<?= number_format((float)$total, '2', '.',''); ?></td>
   </tr><? $total_val += $receipt_value[$r->id]  ?>
 <? endforeach ;?>
   <tr class="info">
    <td class="text-left" colspan=5>Total</td>
    <td class="text-right">&#8369;<?= number_format((float)$total_val, '2','.',''); ?></td>
   </tr>
 <? endif ;?>
</table>
</div>
</div>
<a href="<?= base_url(); ?>receipts/print_report" class="btn btn-block btn-info">Print Menu Items</a>
<a href="<?= base_url(); ?>receipts/print_report2" class="btn btn-block btn-info">Print Sold Items</a>
<script type="text/javascript">
$('.date_select').datepicker();
$('.date_select').datepicker("option", "dateFormat", "yy-mm-dd");
</script>
