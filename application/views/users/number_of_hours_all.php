<form action="" method="post" accept-charset="utf-8">
  <div class="form-group">
	<select name="user" class='form-control'>
          <option value="All">All</option>	
	  <? foreach($users_list as $u) :?>
	    <option value="<?= $u->id ;?>"><?= strtoupper($u->name); ?></option>	
	  <? endforeach ;?>
	</select>
  </div>
  <div class="form-group">
   <input type="text" name="date" placeholder="Date" class="datepicker" />
  </div>
  <div class="form-group">
   <input type="submit" value="Submit" />
  </div>
</form>


<? if(!empty($users)) :?>
<? foreach($users as $u) :?>
<? if(!empty($work_time[$u->id])) :?>
<div clas="row">
<div class="col-md-12">
<div class="page-header">
<h1><?= strtoupper($u->name); ?> <small>Work Hours</small></h1>
</div>
<table class="table table-striped">
<tr>
  <th>Category</th>
  <th>Time</th>
</tr>
<? foreach($work_time[$u->id] as $wt) :?>
<tr>
  <td><?= $wt->category ;?></td>
  <td><?= date('F j, Y | g:i A',strtotime($wt->time_on)) ;?></td>
</tr>
<? endforeach ;?>
</table>
</div>
</div>
<? endif ;?>
<? endforeach ;?>
<? endif ;?>

<? if(!empty($user)) :?>
<? if(!empty($work_time[$user->id])) :?>
<div clas="row">
<div class="col-md-12">
<div class="page-header">
<h1><?= strtoupper($user->name); ?> <small>Work Hours</small></h1>
</div>
<table class="table table-striped">
<tr>
  <th>Category</th>
  <th>Time</th>
</tr>
<? foreach($work_time[$user->id] as $wt) :?>
<tr>
  <td><?= $wt->category ;?></td>
  <td><?= date('F j, Y | g:i A',strtotime($wt->time_on)) ;?></td>
</tr>
<? endforeach ;?>
</table>
</div>
</div>
<? endif ;?>
<? endif ;?>

<script type="text/javascript" charset="utf-8">
$('.datepicker').datepicker();
$('.datepicker').datepicker("option", "dateFormat", "yy-mm-dd");
</script>
