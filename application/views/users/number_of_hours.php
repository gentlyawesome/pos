<div clas="row">
<div class="col-md-12">
<div class="page-header">
<h1><?= strtoupper($user->name); ?> <small>Work Hours</small></h1>
</div>
<table class="table table-striped">
<tr>
  <th>Category</th>
  <th>Time</th>
</tr>
<? if(!empty($work_time)) :?>
<? foreach($work_time as $wt) :?>
<tr>
  <td><?= $wt->category ;?></td>
  <td><?= date('F j, Y | g:i:s A',strtotime($wt->time_on)) ;?></td>
</tr>
<? if($wt->category == "break_out") :?>
<tr>
  <td>Work Hours</td>
  <td><?= $breaks['total_hour_'.$wt->id]; ?></td>
</tr>
<? endif; ?>
<? endforeach ;?>
<? endif ;?>
</table>
</div>
</div>
